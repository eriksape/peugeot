<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dealers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('branch');
            $table->string('city');
            $table->timestamps();
        });

        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model');
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->unsignedInteger('dealer_id')->nullable();
            $table->unsignedInteger('vehicle_id')->nullable();
            $table->date('test_drive')->nullable();
            $table->string('type')->default('user');
            $table->rememberToken();
            $table->timestamps();
            $table->double('quiz_time', 12, 4)->nullable();
            $table->integer('correct_answers')->nullable();
            $table->foreign('dealer_id')->references('id')->on('dealers');
            $table->foreign('vehicle_id')->references('id')->on('vehicles');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('dealers');
        Schema::dropIfExists('vehicles');
    }
}
