<!doctype html>
<html lang="es" class="js_active  vc_mobile  vc_transform  vc_transform ">
<head>
    <meta charset="UTF-8">
    <link rel="icon" type="image/ico" href="{{ asset('favicon.ico') }}"/>
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Días Imperdibles PEUGEOT</title>
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C400italic%2C500%2C700%2C700italic&amp;subset=latin%2Clatin-ext&amp;ver=4.8.6" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('/css/font-awesome/css/font-awesome.min.css') }}" type="text/css" media="all">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
<body class="page-template-default page page-id-192 wp-custom-logo wpb-js-composer js-comp-ver-5.0.1 vc_responsive">

<div id="page" class="page-bg">

    <header id="masthead" role="banner">

        <div class="container">

            <button type="button" class="offcanvas-toggle" data-toggle="offcanvas" style="display:none;">

                <span class="screen-reader-text">Toggle sidebar</span>

                <i class="fa fa-bars"></i>

            </button>

            <div style="width:60%" class="roland_logo">

                <div class="header_left_img">

                    <a href="http://www.peugeot.com.mx/homepage.html"><img src="http://test.vbizemails.com/trivia/wp-content/uploads/2018/04/l2.png"></a>

                </div>

                <div class="header_right_content">

                    <p class="header_line1">GANA UN VIAJE DOBLE A PARÍS</p>

                    <p class="header_line2">PARA DIFRUTAR DEL ROLAND GARROS</p>

                    <p class="header_line3">PARTICIPA DEL 19 AL 28 DE ABRIL</p>

                </div>

            </div>





            <div class="site-meta">

                <a href="http://www.peugeot.com.mx/homepage.html" class="custom-logo-link" rel="home" itemprop="url"><img width="350" height="350" src="http://diasimperdibles.com/wp-content/uploads/2018/04/cropped-logo-1-1.png" class="custom-logo" alt="" itemprop="logo" srcset="http://diasimperdibles.com/wp-content/uploads/2018/04/cropped-logo-1-1.png 350w, http://diasimperdibles.com/wp-content/uploads/2018/04/cropped-logo-1-1-150x150.png 150w, http://diasimperdibles.com/wp-content/uploads/2018/04/cropped-logo-1-1-300x300.png 300w" sizes="(max-width: 350px) 100vw, 350px"></a>



                <div class="site-title ">

                    <a href="http://diasimperdibles.com" title="" rel="home"></a>

                </div>



                <div class="site-description "></div>



            </div>





        </div>

    </header>



    <main id="content" class="site-content">

        <a class="screen-reader-text" href="#primary" title="Skip to content">Skip to content</a>


        <div id="primary">
            <article id="post-192" class="post-192 page type-page status-publish hentry big-header">


                <header>
                    <div class="header-meta">
                        <div class="container">


                            <h2 class="entry-title"><a href="http://diasimperdibles.com/terminos-y-condiciones/" rel="bookmark">TÉRMINOS Y CONDICIONES</a></h2>
                            <div class="entry-meta">


                            </div>

                        </div>
                    </div>
                </header>

                <div class="container">

                    <div class="entry-content centered">
                        <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding" style="position: relative; left: -226px; box-sizing: border-box; width: 1006px;"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p style="font-size: 20px;"><i class="fa fa-angle-left" style="font-size: 27px;"></i><a style="color: #000; text-decoration: none;" href="{{ url()->previous() }}">&nbsp;&nbsp; &nbsp;VOLVER</a></p>

                                                            </div>
                                                        </div>
                                                    </div></div></div><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-6"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div><h2 style="color: #000000;text-align: center" class="vc_custom_heading">TÉRMINOS Y CONDICIONES</h2><div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>
                                        <div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
                                                        <div class="wpb_text_column wpb_content_element ">
                                                            <div class="wpb_wrapper">
                                                                <p>Bases de participación:</p>
                                                                <p><strong>“</strong><strong>GANA UN VIAJE A ROLAND GARROS Y VIVE UNA EXPERIENCIA PEUGEOT</strong><strong>”</strong></p>
                                                                <p class="Default" style="text-align: justify;"><span lang="ES-MX" style="font-family: Arial;">Peugeot, S.A. de C.V. en adelante “Peugeot”, establece las bases y condiciones que regirán la promoción</span><b></b></p>
                                                                <p>La “Promoción”: Es un servicio prestado por medio de Velvet Comunicación S.A. de C.V. que es una empresa independiente de Peugeot.</p>
                                                                <p>Todas y cada una de las acciones de la promoción, artes y premios son de la exclusiva responsabilidad de Velvet Comunicación S.A. de C.V. Al aceptar estos términos y condiciones, se libera a “Peugeot”, contemplando a sus respectivos directores, empleados y agentes, de cualquier controversia, reclamos, demandas, y/o daños (reales o resultantes) de todo tipo, ya sean conocidos o desconocidos, que surjan de, o relacionadas con, el uso de los servicios de Velvet Comunicación S.A. de C.V., incluyendo sin limitación, transacciones incompletas o completas entre usted y Velvet Comunicación S.A. de C.V.</p>
                                                                <p>Los siguientes términos y condiciones se aplican al uso del servicio que Velvet Comunicación S.A. de C.V. le brinda. Si usted tiene alguna pregunta por favor contacte a Velvet Comunicación S.A. de C. V. Ubicada en: Benjamín Hill 134, Colonia Hipódromo&nbsp; Condesa, MÉXICO D. F., C. P. 06170,&nbsp; teléfono (55)5919 8446</p>
                                                                <p>Por favor lea estos términos y condiciones cuidadosamente. Si usted no los acepta o no está de acuerdo con ellos, entonces no debe participar en la dinámica “<strong>GANA UN VIAJE A ROLAND GARROS Y VIVE UNA EXPERIENCIA PEUGEOT</strong>” (en lo sucesivo</p>
                                                                <p>denominado como la “Dinámica”) ya que la participación en la Dinámica implica la aceptación de estos términos y condiciones en su totalidad (en lo sucesivo “Términos y Condiciones”).</p>
                                                                <p><strong>OBJETO DE LA DINAMICA:</strong></p>
                                                                <p>La presente Dinámica consiste en la implementación de la Trivia Digital alojada en el sitio:</p>
                                                                <p>“www.diasimperdibles.com (para uso exclusivo dentro de los Distribuidores Peugeot, dentro de la República Mexicana), se anexa listado y nombres, &nbsp;en donde exclusivamente los usuarios registrados de conformidad a los Términos y Condiciones descritas más adelante, podrán participar para ganarse 2&nbsp; (dos) viajes dobles, para asistir al evento deportivo, para asistir al Torneo ROLAND GARROS, CON ACCESO AL VILLAGE VIP DE PEUGEOT Y TRIBUNA PRINCIPAL PARA EL DIA 7 DE JUNIO del 2018,&nbsp; (en lo sucesivo denominado como “Premio”).</p>
                                                                <p>&nbsp;</p>
                                                                <ol>
                                                                    <li><strong>BASES Y CONDICIONES DE LA PROMOCIÓN</strong></li>
                                                                </ol>
                                                                <p><strong>Descripción:</strong> <strong>Los usuarios podrán participar en la Dinámica de conformidad con lo siguiente:</strong></p>
                                                                <p><strong>Participación exclusiva a través de los Distribuidores Peugeot en la República Mexicana:</strong></p>
                                                                <ul>
                                                                    <li>El participante deberá visitar 1 de los 44 Distribuidores PEUGEOT activos en la República Mexicana.</li>
                                                                    <li>Deberá solicitar su prueba de manejo, de cualquier modelo Peugeot.</li>
                                                                    <li>Después de realizada la prueba de manejo, deberá ingresar al sitio Web&nbsp; <a href="http://www.diasimperdibles.com/">www.diasimperdibles.com</a></li>
                                                                    <li>Deberá registrar sus datos con la totalidad de campos solicitados en el formulario.</li>
                                                                    <li>Deberá contestar la Trivia consistente de 5 preguntas relacionadas con temas de Peugeot y Tenis.</li>
                                                                    <li>Para localizar al Distribuidor PEUGEOT más cercano, favor de consultar la siguiente liga: <a href="http://www.peugeot.com.mx/">www.peugeot.com.mx</a></li>
                                                                </ul>
                                                                <p>&nbsp;</p>
                                                                <p><strong>REGISTRO Y TRIVIA DIGITAL DE LA DINAMICA</strong></p>
                                                                <p>A) Registro:</p>
                                                                <ol>
                                                                    <li style="list-style-type: none;">
                                                                        <ol>
                                                                            <li>Los participantes deberán proporcionar sus datos de contacto durante el registro. Incluyendo los datos que solicite el asesor de ventas Peugeot a la hora de realizar la prueba de manejo.</li>
                                                                            <li>Es importante que el participante concrete el proceso de registro, ya que estos datos de contacto serán indispensables para poder participar en la Dinámica y para poder ser contactado en caso de resultar ganador.</li>
                                                                            <li>La información generada por el participante en el registro será enviada automáticamente a través del formulario ingresado, por tal motivo es importante verificar que todos los datos sean veraces y correctos.</li>
                                                                            <li>Todos los usuarios registrados en el período de la Dinámica y promoción, tendrán derecho a contestar la Trivia Digital de la Dinámica.</li>
                                                                        </ol>
                                                                    </li>
                                                                </ol>
                                                                <p>B) Trivia Digital de la Dinámica:</p>
                                                                <ol>
                                                                    <li>La Trivia Digital consta de 5 (cinco) preguntas aleatorias relacionadas con Peugeot y el Tenis, las cuales tendrán 3 y 4 opciones de respuestas.</li>
                                                                    <li>El participante deberá contestar correctamente la mayor cantidad de preguntas en el menor tiempo posible.</li>
                                                                    <li>Sólo tendrá oportunidad de participar en la Dinámica 1 vez.</li>
                                                                </ol>
                                                                <p>&nbsp;</p>
                                                                <p>C) Parámetros que se tomarán en cuenta para elegir al ganador:</p>
                                                                <ol>
                                                                    <li>Obtener el mayor número de respuestas en el menor tiempo.</li>
                                                                    <li>Se contarán minutos, segundos y milésimas.</li>
                                                                    <li>Confirmación de la realización de prueba de manejo en el Distribuidor Peugeot.</li>
                                                                </ol>
                                                                <p>&nbsp;</p>
                                                                <p>D) Parámetros que se tomarán en cuenta para desempate:</p>
                                                                <ol>
                                                                    <li>En caso de empate, el ganador se determinará por la fecha y horario de registro, quien se haya registrado primero, será el ganador.</li>
                                                                </ol>
                                                                <p>&nbsp;</p>
                                                                <p>E) Una vez concluida Trivia Digital de la Dinámica, se guardará la siguiente información del participante:</p>
                                                                <ol>
                                                                    <li>Datos personales de contacto.</li>
                                                                    <li>Número de respuestas correctas.</li>
                                                                    <li>Tiempo en responder el total de preguntas.</li>
                                                                </ol>
                                                                <p>El Sitio es administrado por Velvet Comunicación, S.A. de C.V. y los datos personales de contacto y/o de identificación de los participantes ingresados durante el registro no serán revelados, ni compartidos a terceras personas sin el consentimiento de los participantes, con excepción de aquellas señaladas en forma expresa en el AVISO DE PRIVACIDAD ubicado en <a href="http://www.velvetcom.mx/avisodeprivacidad">www.velvetcom.mx/avisodeprivacidad</a></p>
                                                                <p>&nbsp;</p>
                                                                <p><strong>PREMIO </strong></p>
                                                                <ul>
                                                                    <li>Sólo habrá 2 participantes que serán acreedores a 1 viaje doble para asistir a un partido del evento deportivo “Torneo ROLAND GARROS, CON ACCESO AL VILLAGE VIP DE PEUGEOT Y TRIBUNA PRINCIPAL PARA EL DIA 7 DE JUNIO del 2018”</li>
                                                                </ul>
                                                                <p><strong>El viaje doble para cada ganador consta de:</strong></p>
                                                                <ul>
                                                                    <li>2 Boletos de acceso para asistir al TORNEO ROLAND GARROS, CON ACCESO AL VILLAGE VIP DE PEUGEOT Y TRIBUNA PRINCIPAL PARA EL DIA 7 DE JUNIO</li>
                                                                    <li>2 Boletos de Avión Clase Turista MEX-PARIS-MEX ( ganador + acompañante) saliendo de la ciudad de México el martes 5 de junio y regresando el sábado 9 de junio a ciudad de México.</li>
                                                                    <li>Hospedaje en Hotel de 3 (tres) estrellas en la Ciudad de PARIS, Francia POR un total de 4 DIAS 3 NOCHES. (entrando el 6 de junio y saliendo el 9 de junio de 2018) con desayuno continental incluido.</li>
                                                                    <li>Traslados terrestre desde el “Aeropuerto CHARLES DE GAULLE” EN PARIS AL HOTEL PARA EL 6 &nbsp;DE JUNIO.</li>
                                                                    <li>Tarjeta con $10,000.00 pesos mexicanos (diez mil pesos 00/100) para viáticos.</li>
                                                                </ul>
                                                                <p><strong>Condiciones:</strong></p>
                                                                <ul>
                                                                    <li>El torneo será elegido por Peugeot y el participante no podrá elegir ni solicitar ninguna modificación.</li>
                                                                    <li>El Premio no incluye, gastos de trámite de visas, pasaportes, ningun tipo de gasto de índole personal, propinas, excursiones, bebidas alcohólicas, servicio a habitación en el hotel ni minibar, y cualquier otro gasto generado o incurrido por el ganador y su acompañante para y durante el aprovechamiento del Premio.</li>
                                                                    <li>Tanto los boletos de avión redondo como el hospedaje, quedan sujetos a disponibilidad.</li>
                                                                    <li>La ciudad de origen considerada en esta Dinámica es la Cd. de México (CDMX), por lo que si el que resultare ganador vive en una ciudad en el interior de la República Mexicana, los gastos de traslado, hospedaje y/o cualquier gasto en el que incurra para llegar o durante su estadía en la CDMX (tanto del ganador como el acompañante), deberán ser cubiertos por el ganador.</li>
                                                                    <li>El Premio no es intercambiable por dinero en efectivo u otros premios sustitutos, descuentos o bonificaciones de ningún tipo ni para ningún servicio que preste Peugeot o cualquiera de sus Distribuidores Autorizados Peugeot.</li>
                                                                    <li>Es necesario contar con pasaporte con vigencia mínima de 6 meses posteriores al término del viaje.</li>
                                                                    <li>Para obtener el premio es obligatorio haber cumplido todas las etapas de la Dinámica y poder demostrar una vez habiendo ganado, su participación.</li>
                                                                    <li>Peugeot México y Velvet verificará y/o confirmará que todo los procesos se hayan realizado.</li>
                                                                    <li>Los resultados oficiales se publicaran en las RRSS de Peugeot y en el sitio <a href="http://www.diasimperdibles.com">diasimperdibles.com</a> el día 7 de mayo.</li>
                                                                    <li>No podrán participar empleados de Peugeot ni empleados de Distribuidores Peugeot o de Velvet Comunicación, S.A. de C.V.</li>
                                                                </ul>
                                                                <p><strong>&nbsp;</strong></p>
                                                                <p><strong>VIGENCIA Y LIMITES DE LA DINAMICA</strong></p>
                                                                <ol start="2018">
                                                                    <li>Los interesados podrán participar exclusivamente del <strong>19 al 28 de abril del 2018.</strong></li>
                                                                    <li>Solo se podrá participar en 1 sola ocasión.</li>
                                                                    <li>Si se encuentra duplicidad de participación en la Dinámica, se eliminará al o a los &nbsp;participantes.</li>
                                                                    <li>Sólo podrán participar personas mayores de 18 años que radiquen en la Cd. de México, Área Metropolitana e Interior de la República Mexicana.</li>
                                                                </ol>
                                                                <p><strong>&nbsp;</strong></p>
                                                                <p><strong>FECHA Y MEDIOS PARA DAR A CONOCER AL GANADOR</strong></p>
                                                                <ol>
                                                                    <li>El ganador se dará a conocer el 7 de mayo de 2018 a través redes sociales de Peugeot en el perfil de Facebook de PEUGEOT; posteriormente personal autorizado de Velvet Comunicación se pondrá en contacto con el ganador para compartir los detalles acerca de cómo redimir el Premio.</li>
                                                                    <li>A su vez, es indispensable que el acompañante del ganador de igual manera cuente con estos documentos oficiales para poder viajar al extranjero (pasaporte vigente con al menos 6 meses de vigencia posterior al disfrute del premio).</li>
                                                                </ol>
                                                                <p>&nbsp;</p>
                                                                <p>&nbsp;</p>
                                                                <p><strong>INFORMACION, IMÁGENES, VIDEOS Y/O FOTOS Y REDES SOCIALES</strong></p>
                                                                <p>Al participar en la Dinámica, los participantes expresamente otorgan una licencia no exclusiva a PEUGEOT para la utilización, reproducción, explotación y comunicación pública de imágenes, videos y/o fotos que resulten de la participación en la presente Dinámica, en cualquiera de sus etapas o durante el aprovechamiento del Premio, para ser utilizadas como contenido en redes sociales, televisión, radio y prensa directa o indirectamente por Peugeot, sus sociedades afiliadas o relacionadas y/o proveedores de servicios con quienes tiene una relación jurídica.</p>
                                                                <p>Así mismo, los participantes y los acompañantes se comprometen a firmar cualquier documento que Peugeot estime pertinente, para la utilización, reproducción, explotación y comunicación pública de imágenes, videos y/o fotos que resulten de la Dinámica o durante el aprovechamiento del Premio, así como de su voz y/o imagen a través de cualquier medio de comunicación conocido o por conocerse.</p>
                                                                <p>Peugeot &nbsp;no recabará información personal considerada como sensible por motivo de la realización de esta Dinámica.</p>
                                                                <p>Durante el viaje, el ganador acepta compartir, es sus propias redes sociales (Facebook, Twitter y/o Instagram) las imágenes relacionadas a la experiencia por haber recibido el Premio, así como realizar el taggeo correspondiente (indicado por Peugeot) para poder amplificar sus imágenes en las redes sociales propias de Peugeot.</p>
                                                                <p>&nbsp;</p>
                                                                <p><strong><u>DISTRIBUIDORES PARTICIPANTES PEUGEOT®:</u></strong></p>
                                                                <ul>
                                                                    <li><strong><u>&nbsp;Peugeot Tijuana, Peugeot Mexicali,</u></strong><u> <strong>Lyon Motors Durango, Peugeot Hermosillo, Peugeot Chihuahua, Peugeot Culiacán, Peugeot Chiapas, Peugeot Oaxaca, Peugeot Campeche, Peugeot Querétaro, Peugeot Pachuca, Peugeot Puebla, Peugeot San Luis Potosí, Peugeot León, Peugeot Felinautos Boca, Peugeot Xalapa, Peugeot Aguascalientes, Peugeot Toluca, Peugeot Saltillo, Peugeot Galojal, Peugeot Morelia, Peugeot Vegsa, Peugeot Refrance Gonzalitos, Eurosurman Tec, Peugeot Refrance San Nicolás, Peugeot Puerto Vallarta, Meridien Mérida, Meridien Cancún, Lyon Motors Torreón, Peugeot Colima, Peugeot Villahermosa, Peugeot Cuernavaca, Peugeot Arboledas, Peugeot Interlomas, Alden Roilion, Afu Universidad, Peugeot Vallejo, Peugeot Anzures, Peugeot Miramontes, Peugeot Tampico, Peugeot Lomas Verdes</strong></u></li>
                                                                </ul>
                                                                <p><strong>VIGENCIA:</strong></p>
                                                                <ul>
                                                                    <li>Del 19 al 28 de abril del 2018.</li>
                                                                </ul>
                                                                <p><strong>LUGAR Y HORARIO PARA RECOGER EL PREMIO</strong></p>
                                                                <p>La entrega de los 2 premios se llevarán a cabo el día VIERNES 1 DE JUNIO de 2018, en la&nbsp; DISTRIBUIDORA PEUGEOT donde haya hecho su participación el ganador en horario de 9:00 a 17:00 horas. Para recibir el premio los ganadores deberán firmar la aceptación el premio en los <strong>Términos y condiciones de la dinámica “</strong><strong>GANA UN VIAJE A ROLAND GARROS Y VIVE UNA EXPERIENCIA PEUGEOT”</strong></p>
                                                                <p>Si hubiera algún cambio en la fecha de entrega del Premio, se informará por escrito al ganador.</p>
                                                                <p><strong>&nbsp;</strong></p>
                                                                <p><strong>DISPOSICIONES VARIAS</strong></p>
                                                                <ol>
                                                                    <li>Peugeot se reserva el derecho de modificar estos Términos y Condiciones en cualquier momento y sin previo aviso.</li>
                                                                    <li>Peugeot promueve la competencia sana y rechaza los ataques relacionados a la no competencia por lo que se entenderá que se produce fraude, cuando enunciativamente más no limitativamente sucede lo siguiente:
                                                                        <ul>
                                                                            <li>1 sola persona participa en la Dinámica con diferentes direcciones de Correo electrónico o alias.</li>
                                                                            <li>El participante proporciona datos falsos o incompletos.</li>
                                                                            <li>Se reproduzca o altere la información dada en la Dinámica.</li>
                                                                            <li>Cualquier intento, abuso o acto de mala fe en el que se aproveche de la Dinámica o se ponga a sí mismo en situación privilegiada, así como cualquier otra actividad que altere la competencia justa por obtener el Premio.</li>
                                                                        </ul>
                                                                    </li>
                                                                    <li>Peugeot tendrá el poder de decisión con respecto a cualquier situación no prevista en estos Términos y Condiciones.</li>
                                                                    <li>Las decisiones de Peugeot sobre el resultado de esta Dinámica son finales y obligatorias en todos aspectos.</li>
                                                                    <li>Cualquier participante que use apoyos prohibidos, que busquen obtener ventaja con cualquier clase de manipulación, o que presenten información falsa serán descalificados automáticamente. En esos casos, Peugeot se reserva el derecho de retirar el Premio.</li>
                                                                    <li>Peugeot se reserva el derecho a rechazar y descalificar cualquier participación que sea considerada ofensiva, violenta, inapropiada o fuera de los valores corporativos de Peugeot a discreción de ésta última.</li>
                                                                </ol>
                                                                <p><strong>LIBERACIÓN DE RESPONSABILIDADES</strong></p>
                                                                <ol>
                                                                    <li>Peugeot no será responsable de que las autoridades migratorias del país al que se viajará (FRANCIA) para disfrutar el Premio, no autoricen el ingresar al país. Tampoco será responsable de que las autoridades mexicanas no autoricen el salir del país.</li>
                                                                    <li>Todas las obligaciones a cargo de Peugeot se considerarán cumplidas al entregar el Premio.</li>
                                                                    <li>Peugeot no se responsabiliza de las posibles pérdidas de datos por problemas de correo electrónico y/o por el mal funcionamiento de Facebook, Twitter y/o Internet.</li>
                                                                    <li>Peugeot se excluye de cualquier responsabilidad por los daños y perjuicios de toda naturaleza que, a pesar de las medidas de seguridad adoptadas, pudieran deberse a la utilización indebida de los servicios y de los contenidos por parte de los usuarios, y, en particular, aunque no de forma exclusiva, por los daños y perjuicios de toda naturaleza que puedan deberse a la Suplantación de la personalidad de un tercero efectuada por un usuario en cualquier clase de comunicación realizada a través del portal.</li>
                                                                    <li>Peugeot se libera de manera enunciativa más no limitativa de cualquier responsabilidad por daños y perjuicios, heridas, accidentes, enfermedades o muerte que resulten de la participación en la Dinámica y del aprovechamiento del Premio.</li>
                                                                    <li>Por el sólo hecho de participar en la Dinámica, los participantes aceptan estos Términos y Condiciones, y se sujetan a ellos.</li>
                                                                    <li>Peugeot tendrá el poder de decisión con respecto a cualquier situación no prevista en estos Términos y Condiciones y las decisiones de PEUGEOT sobre el resultado de esta Dinámica son finales y obligatorias en todos aspectos.</li>
                                                                    <li>Al participar en esta Dinámica, los participantes aceptan que Peugeot y sus miembros, afiliados, subsidiarios, consultores, representantes, contratistas, abogados, publicistas, miembros de relaciones públicas, agencias de marketing, así como sus respectivos agentes, directores, empleados y representantes (las “Partes Liberadas”) no serán responsables por: (i) participaciones, comunicaciones o declaraciones retrasadas, perdidas, robadas, mal dirigidas, incompletas, no leíbles, ininteligibles, o mal representadas, independientemente del método de transmisión; así como por fallas en los sistemas telefónicos, hardware de teléfono o computadora, software u otros errores técnicos o mal funcionamiento, pérdida de conexión, desconexiones, retrasos o errores de transmisión; corrupción de datos, robos y robos de identidad, destrucción, accesos no autorizados o alteración de las participaciones u otros materiales; cualquier lesión, muerte, pérdida o daño que pueda ser causado, directa o indirectamente, en todo o en parte, por la participación en la Dinámica o por el aprovechamiento de los Premio; cualquier accidente o daño ocurrido durante el viaje a Paris; cualquier error de impresión, tipográfico, administrativo o tecnológico en alguno de los materiales asociados a la Dinámica; cualquier evento que resulte por causas de fuerza mayor y por casos fortuitos; y ambigüedad, error, información incorrecta o imprecisa en estos Términos y condiciones o por información incorrecta o imprecisa que sea causada por los Participantes.</li>
                                                                    <li>Los participantes acuerdan renunciar a cualquier derecho que pudieren efectuar como reclamo en contra de las Partes Liberadas y aceptan que las Partes Liberadas no tendrán ningún tipo de responsabilidad y deberán de permanecer indemnes por cualquier tema relacionado directo o indirectamente con la Dinámica.</li>
                                                                </ol>
                                                                <p><strong>TELÉFONO PARA INFORMACIÓN Y ACLARACIONES</strong></p>
                                                                <p>Para cualquier informe o aclaración relacionada con esta Dinámica favor de comunicarse al teléfono 5919 8446 en el horario de atención de 9:00 a 17:00 horas (tiempo de México, Distrito Federal) de lunes a viernes al departamento de Finanzas y Administración.</p>
                                                                <p><strong>ACEPTACIÓN DE LOS PRESENTES TÉRMINOS Y CONDICIONES</strong></p>
                                                                <p>Mediante la participación en la Dinámica en el sitio <a href="http://www.diasimperdibles.com/">www.diasimperdibles.com</a>, se aceptan los presentes Términos y Condiciones de la presente Dinámica, así como la Política de Privacidad que se encuentra en <a href="http://www.diasimperdibles.com.mx">www.peugeot.com.mx</a></p>
                                                                <p>La participación en esta Dinámica, implica el entendimiento y aceptación de lo establecido en estos Términos y Condiciones.</p>
                                                                <p>*La red social FACEBOOK, INC, TWITTER o INSTAGRAM no se crearon ni forman parte de la presente dinámica, por lo que no serán responsables que de cualquier reclamación de derive de la misma.</p>
                                                                <p>&nbsp;</p>
                                                                <p><strong>ADVERTENCIA: CUALQUIER INTENTO DE DAÑAR DELIBERADAMENTE O SOCAVAR LA OPERACIÓN LEGÍTIMA DE LA “PROMOCIÓN”, CONLLEVARÁ A LA DESCALIFICACIÓN DEL PARTICIPANTE EN LA “PROMOCIÓN” PUDIENDO ADEMÁS SUPONER UNA VIOLACIÓN DE LEYES CIVILES Y PENALES Y EN CASO DE PRODUCIRSE UN ATENTADO DE ESTE TIPO, PEUGEOT SE RESERVA EL DERECHO DE RECLAMAR INDEMNIZACIÓN POR DAÑOS Y PERJUICIOS (INCLUIDOS LOS HONORARIOS DE ABOGADOS) CONFORME A LAS DISPOSICIONES LEGALES APLICABLES Y EN CUALQUIERA DE LOS ÁMBITOS DE RESPONSABILIDAD.</strong></p>
                                                                <p><em>“La (s)&nbsp;empresa (s)&nbsp;responsable (s)&nbsp;de la presente Promoción es (son)&nbsp;VELVET COMUNICACIÓN S.A. DE C.V.-&nbsp;Para cualquier aclaración o información referente a esta Promoción o de resultados del mismo, comunicarse en la Ciudad de México al teléfono (5559198446) y desde el interior de la Republica&nbsp;al (01 5559198446) de lunes a viernes de 9:00 a las 19:00 o acudir al domicilio ubicado en (Benjamín Hill 134, Colonia Hipódromo Condesa, Ciudad de México C. P. 06170).”</em></p>

                                                            </div>
                                                        </div>
                                                    </div></div></div><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div></div>
                    </div><!-- .entry-content -->

                    <footer>
                    </footer><!-- .entry -->

                </div>

            </article><!-- #post-192 -->
        </div>




    </main><!-- main -->



    <img src="http://diasimperdibles.com/wp-content/uploads/2018/04/peugeot_w-p.png" class="footer_img">



    <footer id="footer" role="contentinfo">



        <div class="container">



            <div class="col-md-6 share" style="float:left;">



                <p class="social_share">Síguenos en:</p>



                <a href="https://www.facebook.com/peugeot.mexico?ref=ts"><i class="fa fa-facebook-f"></i></a> &nbsp; <a href="https://twitter.com/PeugeotMex"><i class="fa fa-twitter"></i></a> &nbsp; <a href="https://www.instagram.com/peugeotmex/"><img src="http://diasimperdibles.com/wp-content/uploads/2018/04/insta.png" class="insta_img"></a> &nbsp; <a href="https://www.youtube.com/user/peugeotmexico"><i class="fa fa-youtube"></i></a>



            </div>



            <div class="col-md-6 terms" style="float:right" ;="">



                <a href="http://diasimperdibles.com/terminos-y-condiciones/">TÉRMINOS Y CONDICIONES</a>



            </div>






















            <!--	<div class="copyright">



                    <span id="abc-custom-copyright">Copyright &copy; 2018  <a href="http://diasimperdibles.com"></a>. All Rights Reserved.</span>



                    <span id="abc-credit-link" class="abc-credit">&nbsp;&vert;&nbsp;The Byline Theme by <a href="https://alphabetthemes.com/downloads/byline-wordpress-theme">Alphabet Themes</a>.</span>



                </div> -->



        </div>



    </footer><!-- #footer -->



</div>
<!-- #page -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    function vc_js(){vc_toggleBehaviour(),vc_tabsBehaviour(),vc_accordionBehaviour(),vc_teaserGrid(),vc_carouselBehaviour(),vc_slidersBehaviour(),vc_prettyPhoto(),vc_googleplus(),vc_pinterest(),vc_progress_bar(),vc_plugin_flexslider(),vc_google_fonts(),vc_gridBehaviour(),vc_rowBehaviour(),vc_googleMapsPointer(),vc_ttaActivation(),jQuery(document).trigger("vc_js"),window.setTimeout(vc_waypoints,500)}function getSizeName(){var screen_w=jQuery(window).width();return 1170<screen_w?"desktop_wide":960<screen_w&&1169>screen_w?"desktop":768<screen_w&&959>screen_w?"tablet":300<screen_w&&767>screen_w?"mobile":300>screen_w?"mobile_portrait":""}function loadScript(url,$obj,callback){var script=document.createElement("script");script.type="text/javascript",script.readyState&&(script.onreadystatechange=function(){"loaded"!==script.readyState&&"complete"!==script.readyState||(script.onreadystatechange=null,callback())}),script.src=url,$obj.get(0).appendChild(script)}function vc_ttaActivation(){jQuery("[data-vc-accordion]").on("show.vc.accordion",function(e){var $=window.jQuery,ui={};ui.newPanel=$(this).data("vc.accordion").getTarget(),window.wpb_prepare_tab_content(e,ui)})}function vc_accordionActivate(event,ui){if(ui.newPanel.length&&ui.newHeader.length){var $pie_charts=ui.newPanel.find(".vc_pie_chart:not(.vc_ready)"),$round_charts=ui.newPanel.find(".vc_round-chart"),$line_charts=ui.newPanel.find(".vc_line-chart"),$carousel=ui.newPanel.find('[data-ride="vc_carousel"]');"undefined"!=typeof jQuery.fn.isotope&&ui.newPanel.find(".isotope, .wpb_image_grid_ul").isotope("layout"),ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),vc_carouselBehaviour(ui.newPanel),vc_plugin_flexslider(ui.newPanel),$pie_charts.length&&jQuery.fn.vcChat&&$pie_charts.vcChat(),$round_charts.length&&jQuery.fn.vcRoundChart&&$round_charts.vcRoundChart({reload:!1}),$line_charts.length&&jQuery.fn.vcLineChart&&$line_charts.vcLineChart({reload:!1}),$carousel.length&&jQuery.fn.carousel&&$carousel.carousel("resizeAction"),ui.newPanel.parents(".isotope").length&&ui.newPanel.parents(".isotope").each(function(){jQuery(this).isotope("layout")})}}function initVideoBackgrounds(){return window.console&&window.console.warn&&window.console.warn("this function is deprecated use vc_initVideoBackgrounds"),vc_initVideoBackgrounds()}function vc_initVideoBackgrounds(){jQuery("[data-vc-video-bg]").each(function(){var youtubeUrl,youtubeId,$element=jQuery(this);$element.data("vcVideoBg")?(youtubeUrl=$element.data("vcVideoBg"),youtubeId=vcExtractYoutubeId(youtubeUrl),youtubeId&&($element.find(".vc_video-bg").remove(),insertYoutubeVideoAsBackground($element,youtubeId)),jQuery(window).on("grid:items:added",function(event,$grid){$element.has($grid).length&&vcResizeVideoBackground($element)})):$element.find(".vc_video-bg").remove()})}function insertYoutubeVideoAsBackground($element,youtubeId,counter){if("undefined"==typeof YT||"undefined"==typeof YT.Player)return counter="undefined"==typeof counter?0:counter,100<counter?void console.warn("Too many attempts to load YouTube api"):void setTimeout(function(){insertYoutubeVideoAsBackground($element,youtubeId,counter++)},100);var $container=$element.prepend('<div class="vc_video-bg vc_hidden-xs"><div class="inner"></div></div>').find(".inner");new YT.Player($container[0],{width:"100%",height:"100%",videoId:youtubeId,playerVars:{playlist:youtubeId,iv_load_policy:3,enablejsapi:1,disablekb:1,autoplay:1,controls:0,showinfo:0,rel:0,loop:1,wmode:"transparent"},events:{onReady:function(event){event.target.mute().setLoop(!0)}}}),vcResizeVideoBackground($element),jQuery(window).bind("resize",function(){vcResizeVideoBackground($element)})}function vcResizeVideoBackground($element){var iframeW,iframeH,marginLeft,marginTop,containerW=$element.innerWidth(),containerH=$element.innerHeight(),ratio1=16,ratio2=9;containerW/containerH<ratio1/ratio2?(iframeW=containerH*(ratio1/ratio2),iframeH=containerH,marginLeft=-Math.round((iframeW-containerW)/2)+"px",marginTop=-Math.round((iframeH-containerH)/2)+"px",iframeW+="px",iframeH+="px"):(iframeW=containerW,iframeH=containerW*(ratio2/ratio1),marginTop=-Math.round((iframeH-containerH)/2)+"px",marginLeft=-Math.round((iframeW-containerW)/2)+"px",iframeW+="px",iframeH+="px"),$element.find(".vc_video-bg iframe").css({maxWidth:"1000%",marginLeft:marginLeft,marginTop:marginTop,width:iframeW,height:iframeH})}function vcExtractYoutubeId(url){if("undefined"==typeof url)return!1;var id=url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);return null!==id&&id[1]}function vc_googleMapsPointer(){var $=window.jQuery,$wpbGmapsWidget=$(".wpb_gmaps_widget");$wpbGmapsWidget.click(function(){$("iframe",this).css("pointer-events","auto")}),$wpbGmapsWidget.mouseleave(function(){$("iframe",this).css("pointer-events","none")}),$(".wpb_gmaps_widget iframe").css("pointer-events","none")}document.documentElement.className+=" js_active ",document.documentElement.className+="ontouchstart"in document.documentElement?" vc_mobile ":" vc_desktop ",function(){for(var prefix=["-webkit-","-moz-","-ms-","-o-",""],i=0;i<prefix.length;i++)prefix[i]+"transform"in document.documentElement.style&&(document.documentElement.className+=" vc_transform ")}(),"function"!=typeof window.vc_plugin_flexslider&&(window.vc_plugin_flexslider=function($parent){var $slider=$parent?$parent.find(".wpb_flexslider"):jQuery(".wpb_flexslider");$slider.each(function(){var this_element=jQuery(this),sliderSpeed=800,sliderTimeout=1e3*parseInt(this_element.attr("data-interval")),sliderFx=this_element.attr("data-flex_fx"),slideshow=!0;0===sliderTimeout&&(slideshow=!1),this_element.is(":visible")&&this_element.flexslider({animation:sliderFx,slideshow:slideshow,slideshowSpeed:sliderTimeout,sliderSpeed:sliderSpeed,smoothHeight:!0})})}),"function"!=typeof window.vc_googleplus&&(window.vc_googleplus=function(){0<jQuery(".wpb_googleplus").length&&!function(){var po=document.createElement("script");po.type="text/javascript",po.async=!0,po.src="//apis.google.com/js/plusone.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(po,s)}()}),"function"!=typeof window.vc_pinterest&&(window.vc_pinterest=function(){0<jQuery(".wpb_pinterest").length&&!function(){var po=document.createElement("script");po.type="text/javascript",po.async=!0,po.src="//assets.pinterest.com/js/pinit.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(po,s)}()}),"function"!=typeof window.vc_progress_bar&&(window.vc_progress_bar=function(){"undefined"!=typeof jQuery.fn.waypoint&&jQuery(".vc_progress_bar").waypoint(function(){jQuery(this).find(".vc_single_bar").each(function(index){var $this=jQuery(this),bar=$this.find(".vc_bar"),val=bar.data("percentage-value");setTimeout(function(){bar.css({width:val+"%"})},200*index)})},{offset:"85%"})}),"function"!=typeof window.vc_waypoints&&(window.vc_waypoints=function(){"undefined"!=typeof jQuery.fn.waypoint&&jQuery(".wpb_animate_when_almost_visible:not(.wpb_start_animation)").waypoint(function(){jQuery(this).addClass("wpb_start_animation animated")},{offset:"85%"})}),"function"!=typeof window.vc_toggleBehaviour&&(window.vc_toggleBehaviour=function($el){function event(e){e&&e.preventDefault&&e.preventDefault();var title=jQuery(this),element=title.closest(".vc_toggle"),content=element.find(".vc_toggle_content");element.hasClass("vc_toggle_active")?content.slideUp({duration:300,complete:function(){element.removeClass("vc_toggle_active")}}):content.slideDown({duration:300,complete:function(){element.addClass("vc_toggle_active")}})}$el?$el.hasClass("vc_toggle_title")?$el.unbind("click").click(event):$el.find(".vc_toggle_title").unbind("click").click(event):jQuery(".vc_toggle_title").unbind("click").on("click",event)}),"function"!=typeof window.vc_tabsBehaviour&&(window.vc_tabsBehaviour=function($tab){if(jQuery.ui){var $call=$tab||jQuery(".wpb_tabs, .wpb_tour"),ver=jQuery.ui&&jQuery.ui.version?jQuery.ui.version.split("."):"1.10",old_version=1===parseInt(ver[0])&&9>parseInt(ver[1]);$call.each(function(index){var $tabs,interval=jQuery(this).attr("data-interval"),tabs_array=[];if($tabs=jQuery(this).find(".wpb_tour_tabs_wrapper").tabs({show:function(event,ui){wpb_prepare_tab_content(event,ui)},beforeActivate:function(event,ui){1!==ui.newPanel.index()&&ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")},activate:function(event,ui){wpb_prepare_tab_content(event,ui)}}),interval&&0<interval)try{$tabs.tabs("rotate",1e3*interval)}catch(e){window.console&&window.console.log&&console.log(e)}jQuery(this).find(".wpb_tab").each(function(){tabs_array.push(this.id)}),jQuery(this).find(".wpb_tabs_nav li").click(function(e){return e.preventDefault(),old_version?$tabs.tabs("select",jQuery("a",this).attr("href")):$tabs.tabs("option","active",jQuery(this).index()),!1}),jQuery(this).find(".wpb_prev_slide a, .wpb_next_slide a").click(function(e){if(e.preventDefault(),old_version){var index=$tabs.tabs("option","selected");jQuery(this).parent().hasClass("wpb_next_slide")?index++:index--,0>index?index=$tabs.tabs("length")-1:index>=$tabs.tabs("length")&&(index=0),$tabs.tabs("select",index)}else{var index=$tabs.tabs("option","active"),length=$tabs.find(".wpb_tab").length;index=jQuery(this).parent().hasClass("wpb_next_slide")?index+1>=length?0:index+1:0>index-1?length-1:index-1,$tabs.tabs("option","active",index)}})})}}),"function"!=typeof window.vc_accordionBehaviour&&(window.vc_accordionBehaviour=function(){jQuery(".wpb_accordion").each(function(index){var $tabs,$this=jQuery(this),active_tab=($this.attr("data-interval"),!isNaN(jQuery(this).data("active-tab"))&&0<parseInt($this.data("active-tab"))&&parseInt($this.data("active-tab"))-1),collapsible=!1===active_tab||"yes"===$this.data("collapsible");$tabs=$this.find(".wpb_accordion_wrapper").accordion({header:"> div > h3",autoHeight:!1,heightStyle:"content",active:active_tab,collapsible:collapsible,navigation:!0,activate:vc_accordionActivate,change:function(event,ui){"undefined"!=typeof jQuery.fn.isotope&&ui.newContent.find(".isotope").isotope("layout"),vc_carouselBehaviour(ui.newPanel)}}),!0===$this.data("vcDisableKeydown")&&($tabs.data("uiAccordion")._keydown=function(){})})}),"function"!=typeof window.vc_teaserGrid&&(window.vc_teaserGrid=function(){var layout_modes={fitrows:"fitRows",masonry:"masonry"};jQuery(".wpb_grid .teaser_grid_container:not(.wpb_carousel), .wpb_filtered_grid .teaser_grid_container:not(.wpb_carousel)").each(function(){var $container=jQuery(this),$thumbs=$container.find(".wpb_thumbnails"),layout_mode=$thumbs.attr("data-layout-mode");$thumbs.isotope({itemSelector:".isotope-item",layoutMode:"undefined"==typeof layout_modes[layout_mode]?"fitRows":layout_modes[layout_mode]}),$container.find(".categories_filter a").data("isotope",$thumbs).click(function(e){e.preventDefault();var $thumbs=jQuery(this).data("isotope");jQuery(this).parent().parent().find(".active").removeClass("active"),jQuery(this).parent().addClass("active"),$thumbs.isotope({filter:jQuery(this).attr("data-filter")})}),jQuery(window).bind("load resize",function(){$thumbs.isotope("layout")})})}),"function"!=typeof window.vc_carouselBehaviour&&(window.vc_carouselBehaviour=function($parent){var $carousel=$parent?$parent.find(".wpb_carousel"):jQuery(".wpb_carousel");$carousel.each(function(){var $this=jQuery(this);if(!0!==$this.data("carousel_enabled")&&$this.is(":visible")){$this.data("carousel_enabled",!0);var carousel_speed=(getColumnsCount(jQuery(this)),500);jQuery(this).hasClass("columns_count_1")&&(carousel_speed=900);var carousele_li=jQuery(this).find(".wpb_thumbnails-fluid li");carousele_li.css({"margin-right":carousele_li.css("margin-left"),"margin-left":0});var fluid_ul=jQuery(this).find("ul.wpb_thumbnails-fluid");fluid_ul.width(fluid_ul.width()+300),jQuery(window).resize(function(){var before_resize=screen_size;screen_size=getSizeName(),before_resize!=screen_size&&window.setTimeout("location.reload()",20)})}})}),"function"!=typeof window.vc_slidersBehaviour&&(window.vc_slidersBehaviour=function(){jQuery(".wpb_gallery_slides").each(function(index){var $imagesGrid,this_element=jQuery(this);if(this_element.hasClass("wpb_slider_nivo")){var sliderSpeed=800,sliderTimeout=1e3*this_element.attr("data-interval");0===sliderTimeout&&(sliderTimeout=9999999999),this_element.find(".nivoSlider").nivoSlider({effect:"boxRainGrow,boxRain,boxRainReverse,boxRainGrowReverse",slices:15,boxCols:8,boxRows:4,animSpeed:sliderSpeed,pauseTime:sliderTimeout,startSlide:0,directionNav:!0,directionNavHide:!0,controlNav:!0,keyboardNav:!1,pauseOnHover:!0,manualAdvance:!1,prevText:"Prev",nextText:"Next"})}else this_element.hasClass("wpb_image_grid")&&(jQuery.fn.imagesLoaded?$imagesGrid=this_element.find(".wpb_image_grid_ul").imagesLoaded(function(){$imagesGrid.isotope({itemSelector:".isotope-item",layoutMode:"fitRows"})}):this_element.find(".wpb_image_grid_ul").isotope({itemSelector:".isotope-item",layoutMode:"fitRows"}))})}),"function"!=typeof window.vc_prettyPhoto&&(window.vc_prettyPhoto=function(){try{jQuery&&jQuery.fn&&jQuery.fn.prettyPhoto&&jQuery('a.prettyphoto, .gallery-icon a[href*=".jpg"]').prettyPhoto({animationSpeed:"normal",hook:"data-rel",padding:15,opacity:.7,showTitle:!0,allowresize:!0,counter_separator_label:"/",hideflash:!1,deeplinking:!1,modal:!1,callback:function(){var url=location.href;url.indexOf("#!prettyPhoto")>-1&&(location.hash="")},social_tools:""})}catch(err){window.console&&window.console.log&&console.log(err)}}),"function"!=typeof window.vc_google_fonts&&(window.vc_google_fonts=function(){return!1}),window.vcParallaxSkroll=!1,"function"!=typeof window.vc_rowBehaviour&&(window.vc_rowBehaviour=function(){function fullWidthRow(){var $elements=$('[data-vc-full-width="true"]');$.each($elements,function(key,item){var $el=$(this);$el.addClass("vc_hidden");var $el_full=$el.next(".vc_row-full-width");if($el_full.length||($el_full=$el.parent().next(".vc_row-full-width")),$el_full.length){var el_margin_left=parseInt($el.css("margin-left"),10),el_margin_right=parseInt($el.css("margin-right"),10),offset=0-$el_full.offset().left-el_margin_left,width=$(window).width();if($el.css({position:"relative",left:offset,"box-sizing":"border-box",width:$(window).width()}),!$el.data("vcStretchContent")){var padding=-1*offset;0>padding&&(padding=0);var paddingRight=width-padding-$el_full.width()+el_margin_left+el_margin_right;0>paddingRight&&(paddingRight=0),$el.css({"padding-left":padding+"px","padding-right":paddingRight+"px"})}$el.attr("data-vc-full-width-init","true"),$el.removeClass("vc_hidden"),$(document).trigger("vc-full-width-row-single",{el:$el,offset:offset,marginLeft:el_margin_left,marginRight:el_margin_right,elFull:$el_full,width:width})}}),$(document).trigger("vc-full-width-row",$elements)}function parallaxRow(){var vcSkrollrOptions,callSkrollInit=!1;return window.vcParallaxSkroll&&window.vcParallaxSkroll.destroy(),$(".vc_parallax-inner").remove(),$("[data-5p-top-bottom]").removeAttr("data-5p-top-bottom data-30p-top-bottom"),$("[data-vc-parallax]").each(function(){var skrollrSpeed,skrollrSize,skrollrStart,skrollrEnd,$parallaxElement,parallaxImage,youtubeId;callSkrollInit=!0,"on"===$(this).data("vcParallaxOFade")&&$(this).children().attr("data-5p-top-bottom","opacity:0;").attr("data-30p-top-bottom","opacity:1;"),skrollrSize=100*$(this).data("vcParallax"),$parallaxElement=$("<div />").addClass("vc_parallax-inner").appendTo($(this)),$parallaxElement.height(skrollrSize+"%"),parallaxImage=$(this).data("vcParallaxImage"),youtubeId=vcExtractYoutubeId(parallaxImage),youtubeId?insertYoutubeVideoAsBackground($parallaxElement,youtubeId):"undefined"!=typeof parallaxImage&&$parallaxElement.css("background-image","url("+parallaxImage+")"),skrollrSpeed=skrollrSize-100,skrollrStart=-skrollrSpeed,skrollrEnd=0,$parallaxElement.attr("data-bottom-top","top: "+skrollrStart+"%;").attr("data-top-bottom","top: "+skrollrEnd+"%;")}),!(!callSkrollInit||!window.skrollr)&&(vcSkrollrOptions={forceHeight:!1,smoothScrolling:!1,mobileCheck:function(){return!1}},window.vcParallaxSkroll=skrollr.init(vcSkrollrOptions),window.vcParallaxSkroll)}function fullHeightRow(){var $element=$(".vc_row-o-full-height:first");if($element.length){var $window,windowHeight,offsetTop,fullHeight;$window=$(window),windowHeight=$window.height(),offsetTop=$element.offset().top,offsetTop<windowHeight&&(fullHeight=100-offsetTop/(windowHeight/100),$element.css("min-height",fullHeight+"vh"))}$(document).trigger("vc-full-height-row",$element)}function fixIeFlexbox(){var ua=window.navigator.userAgent,msie=ua.indexOf("MSIE ");(msie>0||navigator.userAgent.match(/Trident.*rv\:11\./))&&$(".vc_row-o-full-height").each(function(){"flex"===$(this).css("display")&&$(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')})}var $=window.jQuery;$(window).off("resize.vcRowBehaviour").on("resize.vcRowBehaviour",fullWidthRow).on("resize.vcRowBehaviour",fullHeightRow),fullWidthRow(),fullHeightRow(),fixIeFlexbox(),vc_initVideoBackgrounds(),parallaxRow()}),"function"!=typeof window.vc_gridBehaviour&&(window.vc_gridBehaviour=function(){jQuery.fn.vcGrid&&jQuery("[data-vc-grid]").vcGrid()}),"function"!=typeof window.getColumnsCount&&(window.getColumnsCount=function(el){for(var find=!1,i=1;!1===find;){if(el.hasClass("columns_count_"+i))return find=!0,i;i++}});var screen_size=getSizeName();"function"!=typeof window.wpb_prepare_tab_content&&(window.wpb_prepare_tab_content=function(event,ui){var $ui_panel,$google_maps,panel=ui.panel||ui.newPanel,$pie_charts=panel.find(".vc_pie_chart:not(.vc_ready)"),$round_charts=panel.find(".vc_round-chart"),$line_charts=panel.find(".vc_line-chart"),$carousel=panel.find('[data-ride="vc_carousel"]');if(vc_carouselBehaviour(),vc_plugin_flexslider(panel),ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),panel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&panel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),$pie_charts.length&&jQuery.fn.vcChat&&$pie_charts.vcChat(),$round_charts.length&&jQuery.fn.vcRoundChart&&$round_charts.vcRoundChart({reload:!1}),$line_charts.length&&jQuery.fn.vcLineChart&&$line_charts.vcLineChart({reload:!1}),$carousel.length&&jQuery.fn.carousel&&$carousel.carousel("resizeAction"),$ui_panel=panel.find(".isotope, .wpb_image_grid_ul"),$google_maps=panel.find(".wpb_gmaps_widget"),0<$ui_panel.length&&$ui_panel.isotope("layout"),$google_maps.length&&!$google_maps.is(".map_ready")){var $frame=$google_maps.find("iframe");$frame.attr("src",$frame.attr("src")),$google_maps.addClass("map_ready")}panel.parents(".isotope").length&&panel.parents(".isotope").each(function(){jQuery(this).isotope("layout")})}),"function"!=typeof window.vc_googleMapsPointer,jQuery(document).ready(function($){window.vc_js()});
</script>
<script>
    $(function(){ vc_js(); });
</script>

</body>
</html>