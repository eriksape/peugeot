@extends('layouts.app')

@section('content')
    <div class="container">
        <table id="users">
            <thead>
            <tr>
                <td>Nombre</td>
                <td>Email</td>
                <td>Día de prueba</td>
                <td>Distribuidor</td>
                <td>Vechiculo</td>
                <td>Aciertos</td>
                <td>Tiempo</td>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>

@endsection


@section('scripts')
    <script>
        var api;
        $("#users").DataTable({
            processing: true,
            serverSide: true,
            dom: 'T<"clear">lfrtip',
            ajax: {
                url: '/users/quiz',
                beforeSend: function (request) {
                    var token = $('meta[name=csrf-token]').attr('content');
                    request.setRequestHeader("Authorization", 'Bearer '+token);
                },
                data: function(data){
                    return data;
                },
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email' },
                {data: 'test_drive', name: 'test_drive' },
                {data: 'dealer.branch', name: 'dealer.branch' },
                {data: 'vehicle.model', name: 'vehicle.model' },
                {data: 'correct_answers', name: 'correct_answers' },
                {data: 'quiz_time', name: 'quiz_time' },
            ],
        });
    </script>
@endsection
