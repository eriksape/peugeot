<!doctype html>
<html lang="en-US" class=" js_active  vc_desktop  vc_transform  vc_transform ">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quiz</title>
    <link href="https://fonts.googleapis.com/css?family=Fredoka+One" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto%3A300%2C400%2C400italic%2C500%2C700%2C700italic&amp;subset=latin%2Clatin-ext&amp;ver=4.8.6" type="text/css" media="all">
    <link rel="stylesheet" href="{{ asset('/css/font-awesome/css/font-awesome.min.css') }}" type="text/css" media="all">
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('/css/main.css') }}" rel="stylesheet" type="text/css">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="page-template-default page page-id-18 wp-custom-logo wpb-js-composer js-comp-ver-5.0.1 vc_responsive">
<div id="fb-root"></div>
<div id="page" class="page-bg">
    <header id="masthead" role="banner">
        <div class="container">
            <button type="button" class="offcanvas-toggle" data-toggle="offcanvas" style="display:none;">
                <span class="screen-reader-text">Toggle sidebar</span>
                <i class="fa fa-bars"></i>
            </button>
            <div style="width:60%" class="roland_logo">
                <div class="header_left_img">
                    <a href="http://www.peugeot.com.mx/homepage.html"><img src="http://test.vbizemails.com/trivia/wp-content/uploads/2018/04/l2.png"></a>
                </div>
                <div class="header_right_content">
                    <p class="header_line1">GANA UN VIAJE DOBLE A PARÍS</p>
                    <p class="header_line2">PARA DIFRUTAR DEL ROLAND GARROS</p>
                    <p class="header_line3">PARTICIPA DEL 19 AL 28 DE ABRIL</p>
                </div>
            </div>
            <div class="site-meta">
                <a href="http://www.peugeot.com.mx/homepage.html" class="custom-logo-link" rel="home" itemprop="url"><img width="350" height="350" src="http://diasimperdibles.com/wp-content/uploads/2018/04/cropped-logo-1-1.png" class="custom-logo" alt="" itemprop="logo" srcset="http://diasimperdibles.com/wp-content/uploads/2018/04/cropped-logo-1-1.png 350w, http://diasimperdibles.com/wp-content/uploads/2018/04/cropped-logo-1-1-150x150.png 150w, http://diasimperdibles.com/wp-content/uploads/2018/04/cropped-logo-1-1-300x300.png 300w" sizes="(max-width: 350px) 100vw, 350px"></a>
                <div class="site-title ">
                    <a href="http://diasimperdibles.com" title="" rel="home"></a>
                </div>
                <div class="site-description "></div>
            </div>
        </div>
    </header>
    <main id="content" class="site-content">
        <a class="screen-reader-text" href="#primary" title="Skip to content">Skip to content</a>
        <div id="primary">
            <article id="post-18" class="post-18 page type-page status-publish hentry big-header">
                <header>
                    <div class="header-meta">
                        <div class="container">
                            <h1 class="entry-title">00:01:20</h1>
                            <div class="entry-meta">
                            </div>
                        </div>
                    </div>
                </header>
                <div class="container">
                    <div class="entry-content centered">
                        <div data-vc-full-width="true" data-vc-full-width-init="true" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding" style="position: relative; left: -353.5px; box-sizing: border-box; width: 1393px;">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="wpb_text_column wpb_content_element ">
                                            <div class="wpb_wrapper">
                                                <!-- wp quiz -->
                                                <div class="wq_quizCtr multiple trivia_quiz" data-current-user="0" data-current-question="0" data-questions-answered="0" data-questions="5" data-transition_in="fade" data-transition_out="fade" data-correct-answered="0" data-force-action="0" data-quiz-pid="85" data-share-url="http://diasimperdibles.com/quiz/" data-post-title="Quiz" data-retake-quiz="0" data-question-layout="multiple" data-featured-image="" data-excerpt="" data-ajax-url="http://diasimperdibles.com/wp-admin/admin-ajax.php" data-auto-scroll="1" data-end-answers="false" data-quiz-timer="">
                                                    <div class="wq_quizProgressBarCtr">
                                                        <!-- progress bar -->
                                                        <div class="wq_quizProgressBarCtr" style="display:block">
                                                            <div class="wq_quizProgressBar">
                                                                <span style="background-color:" class="wq_quizProgressValue"></span>
                                                            </div>
                                                        </div>
                                                        <!--// progress bar-->
                                                    </div>
                                                    <div class="wq_questionsCtr">
                                                        <div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:block;">
                                                            <div class="wq_singleQuestionCtr">
                                                                <div class="wq_questionTextWrapper quiz-pro-clearfix">
                                                                    <div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#ffffff;">
                                                                        <h4>Selecciona el i-cockpit que corresponde a Peugeot</h4>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_questionMediaCtr">
                                                                </div>
                                                                <div class="wq_questionAnswersCtr">
                                                                    <div class="wq_answersWrapper">
                                                                        <div class="row">
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="1" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/2018_peugeot_3008_overseas_10.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/audi-a1-interior-2.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/wingle-5-tablero-neoauto-blog.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_triviaQuestionExplanation">
                                                                    <div class="wq_ExplanationHead"></div>
                                                                    <p class="wq_QuestionExplanationText"></p>
                                                                </div>
                                                            </div>
                                                            <div class="wq_continue" style="display:none;">
                                                                <button class="wq_btn-continue" style="background-color:">Continuar &gt;&gt;</button>
                                                            </div>
                                                        </div>
                                                        <div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none;">
                                                            <div class="wq_singleQuestionCtr">
                                                                <div class="wq_questionTextWrapper quiz-pro-clearfix">
                                                                    <div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#ffffff;">
                                                                        <h4>Selecciona los faros traseros correctos de las SUV Peugeot</h4>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_questionMediaCtr">
                                                                </div>
                                                                <div class="wq_questionAnswersCtr">
                                                                    <div class="wq_answersWrapper">
                                                                        <div class="row">
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/Chevrolet-Trailblazer-faros-traseros.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/prueba_Nissan-Qashqai-1.2-DIG-T-Tekna_detalle-trasero.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="1" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/Peugeot_3008_2017_9cc58-1200-800.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_triviaQuestionExplanation">
                                                                    <div class="wq_ExplanationHead"></div>
                                                                    <p class="wq_QuestionExplanationText"></p>
                                                                </div>
                                                            </div>
                                                            <div class="wq_continue" style="display:none;">
                                                                <button class="wq_btn-continue" style="background-color:">Continuar &gt;&gt;</button>
                                                            </div>
                                                        </div>
                                                        <div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none;">
                                                            <div class="wq_singleQuestionCtr">
                                                                <div class="wq_questionTextWrapper quiz-pro-clearfix">
                                                                    <div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#ffffff;">
                                                                        <h4>¿Con qué auto Peugeot obtuvo el 3er éxito consecutivo en la 40va carrera Dakar?</h4>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_questionMediaCtr">
                                                                </div>
                                                                <div class="wq_questionAnswersCtr">
                                                                    <div class="wq_answersWrapper">
                                                                        <div class="row">
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/franjas-sticker-decal-vw-jeans-vochosedan-D_NQ_NP_23108-MLM20241882812_022015-F.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/z-02.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="1" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/thumb_13865_default_large.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_triviaQuestionExplanation">
                                                                    <div class="wq_ExplanationHead"></div>
                                                                    <p class="wq_QuestionExplanationText"></p>
                                                                </div>
                                                            </div>
                                                            <div class="wq_continue" style="display:none;">
                                                                <button class="wq_btn-continue" style="background-color:">Continuar &gt;&gt;</button>
                                                            </div>
                                                        </div>
                                                        <div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none;">
                                                            <div class="wq_singleQuestionCtr">
                                                                <div class="wq_questionTextWrapper quiz-pro-clearfix">
                                                                    <div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#ffffff;">
                                                                        <h4>¿Dónde se celebra el torneo de tenis Roland Garros?</h4>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_questionMediaCtr">
                                                                </div>
                                                                <div class="wq_questionAnswersCtr">
                                                                    <div class="wq_answersWrapper">
                                                                        <div class="row">
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/cloud-sky-new-york-monument-statue-statue-of-liberty-12015-pxhere.com_.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/architecture-building-city-tower-landmark-ride-1229765-pxhere.com_.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="1" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/architecture-sunset-night-city-eiffel-tower-paris-1046619-pxhere.com_.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_triviaQuestionExplanation">
                                                                    <div class="wq_ExplanationHead"></div>
                                                                    <p class="wq_QuestionExplanationText"></p>
                                                                </div>
                                                            </div>
                                                            <div class="wq_continue" style="display:none;">
                                                                <button class="wq_btn-continue" style="background-color:">Continuar &gt;&gt;</button>
                                                            </div>
                                                        </div>
                                                        <div class="wq_singleQuestionWrapper wq_IsTrivia" style="display:none;">
                                                            <div class="wq_singleQuestionCtr">
                                                                <div class="wq_questionTextWrapper quiz-pro-clearfix">
                                                                    <div class="wq_questionTextCtr" style="background-color:#f2f2f2; color:#ffffff;">
                                                                        <h4>¿Cuál es el sedán de la marca Peugeot?</h4>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_questionMediaCtr">
                                                                </div>
                                                                <div class="wq_questionAnswersCtr">
                                                                    <div class="wq_answersWrapper">
                                                                        <div class="row">
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/Partner.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="1" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/PEUGEOT_301.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-wq-4">
                                                                                <div class="wq_singleAnswerCtr wq_IsTrivia wq_hasImage" data-crt="0" style="background-color:#f2f2f2; color:#ffffff;">
                                                                                    <div class="wq_answerImgCtr"><img src="{{ asset('quizimg/peugeot-208-2015-685-en1.196215.18.jpg') }}"></div>
                                                                                    <label class="wq_answerTxtCtr">&nbsp;</label>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row"></div>
                                                                    </div>
                                                                </div>
                                                                <div class="wq_triviaQuestionExplanation">
                                                                    <div class="wq_ExplanationHead"></div>
                                                                    <p class="wq_QuestionExplanationText"></p>
                                                                </div>
                                                            </div>
                                                            <div class="wq_continue" style="display:none;">
                                                                <button class="wq_btn-continue" style="background-color:">Continuar &gt;&gt;</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wq_resultsCtr">
                                                        <div style="display:none;" class="wq_singleResultWrapper wq_IsTrivia" data-id="0" data-min="0" data-max="5">
                                                            <div class="stop" style="text-align:center;">
                                                                <button id="start" style="display:none;">start</button>
                                                                <button id="stop" onclick="clicked()">Finalizar</button>
                                                                <button id="clear" style="display:none;">clear</button>
                                                                <form action="wp-quiz-pro.php" method="post">
                                                                    <input type="hidden" value="00:01:20" id="hiddenstore" name="hiddenstore">
                                                                    <input type="hidden" value="0" id="cur_user" name="cur_user">
                                                                </form>
                                                            </div>
                                                            <div id="score_block" style="display:none">
                                                                <span class="wq_quizTitle">¡GRACIAS POR PARTICIPAR!</span>
                                                                <span class="wq_quizTitle">SCORE</span>
                                                                <div class="wq_resultScoreCtr"></div>
                                                                <span class="wq_quizTitle">TIEMPO</span>
                                                                <span class="result_time" id="result_time"> </span>
                                                                <span style="    text-align: center;
                                                      color: #cacaca;
                                                      width: 100%;
                                                      display: block;
                                                      font-weight: normal;">Los resultados se publicarán en <a style="color: #fff;
                                                      font-weight: bold;
                                                      text-decoration: none;" href="http://facebook.com/peugeot.mexico" target="_blank">facebook.com/peugeot.mexico</a> <br>el día 7 de mayo de 2018</span>
                                                                <div class="footer-top">
                                                                    <div class="footer-top1">
                                                                        <p>
                                                                            <a href="{{ route('conditions') }}">Consulta bases aqui</a></p>
                                                                    </div>
                                                                    <div class="footer-top2">
                                                                        <p>Visita <a href="http://www.peugeot.com.mx" target="_blank" style="text-decoration:none;color:#727070;">www.peugeot.com.mx</a></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="wq_resultTitle"><strong>asasd</strong></div>
                                                            <div class="wq_resultDesc"></div>
                                                            <!-- social share -->
                                                            <div class="wq_shareCtr"></div>
                                                            <!--// social share-->
                                                        </div>
                                                    </div>
                                                    <!-- force action -->
                                                    <div class="wq_quizForceActionCtr" style="display:none;">
                                                        <div class="wq_quizEmailCtr" style="display:none;">
                                                            <form id="wq_infoForm" action="" method="post">
                                                                <p>Just tell us who you are to view your results !</p>
                                                                <div><label>Your first name :</label><input type="text" id="wq_inputName"></div>
                                                                <div><label>Your email address :</label><input type="email" id="wq_inputEmail"></div>
                                                                <p><button type="submit" id="" style="background:">Show my results &gt;&gt;</button></p>
                                                            </form>
                                                        </div>
                                                        <div class="wq_quizForceShareCtr" style="display:none;">
                                                            <p>Please share this quiz to view your results . </p>
                                                            <button class="wq_forceShareFB"><i class="fa fa-facebook icon"></i><span>Facebook</span></button>
                                                        </div>
                                                    </div>
                                                    <!--// force action-->
                                                    <!-- quiz timer -->
                                                    <div class="timerPlaceholder"></div>
                                                    <!--// quiz timer-->
                                                    <!-- embed code -->
                                                    <!--// embed code -->
                                                    <!-- promote link -->
                                                    <!--// promote link-->
                                                    <!-- retake button -->
                                                    <div class="wq_retakeQuizCtr">
                                                        <button style="display:none;" class="wq_retakeQuizBtn"><i class="fa fa-undo"></i>&nbsp; PLAY AGAIN !</button>
                                                    </div>
                                                    <!--// retake button-->
                                                </div>
                                                <!--// wp quiz-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                    </div>
                    <!-- .entry-content -->
                    <footer>
                    </footer>
                    <!-- .entry -->
                </div>
            </article>
            <!-- #post-18 -->
        </div>
    </main>
    <!-- main -->
    <a href="http://www.peugeot.com.mx"><img src="{{ asset('/img/peugeot_w-p.png') }}" class="footer_img"></a>
    <footer id="footer" role="contentinfo">
        <div class="container">
            <div class="col-md-6 share" style="float:left;">
                <p class="social_share">Síguenos en:</p>
                <a href="https://www.facebook.com/peugeot.mexico?ref=ts">
                    <i class="fa fa-facebook-f"></i>
                </a> &nbsp; <a href="https://twitter.com/PeugeotMex">
                    <i class="fa fa-twitter"></i>
                </a> &nbsp; <a href="https://www.instagram.com/peugeotmex/">
                    <i class="fa fa-instagram"></i>
                </a> &nbsp; <a href="https://www.youtube.com/user/peugeotmexico">
                    <i class="fa fa-youtube"></i>
                </a>
            </div>
            <div class="col-md-6 terms" style="float:right" ;="">
                <a href="{{ route('conditions') }}">TÉRMINOS Y CONDICIONES</a>
            </div>
            <!--	<div class="copyright">
               <span id="abc-custom-copyright">Copyright &copy; 2018  <a href="http://diasimperdibles.com"></a>. All Rights Reserved.</span>



               <span id="abc-credit-link" class="abc-credit">&nbsp;&vert;&nbsp;The Byline Theme by <a href="https://alphabetthemes.com/downloads/byline-wordpress-theme">Alphabet Themes</a>.</span>



               </div> -->
        </div>
    </footer>
    <!-- #footer -->
</div>
<script>

    /* <![CDATA[ */
    var wq_l10n = {"correct":"Correct !","wrong":"Wrong !","captionTrivia":"%%score%% \/ %%total%%","captionTriviaFB":"I got %%score%% out of %%total%%, and you?","youVoted":"You voted","nonce":"060933fb8a"};
    /* ]]> */
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('js/transition.js') }}"></script>
<script>
    function vc_js(){vc_toggleBehaviour(),vc_tabsBehaviour(),vc_accordionBehaviour(),vc_teaserGrid(),vc_carouselBehaviour(),vc_slidersBehaviour(),vc_prettyPhoto(),vc_googleplus(),vc_pinterest(),vc_progress_bar(),vc_plugin_flexslider(),vc_google_fonts(),vc_gridBehaviour(),vc_rowBehaviour(),vc_googleMapsPointer(),vc_ttaActivation(),jQuery(document).trigger("vc_js"),window.setTimeout(vc_waypoints,500)}function getSizeName(){var screen_w=jQuery(window).width();return 1170<screen_w?"desktop_wide":960<screen_w&&1169>screen_w?"desktop":768<screen_w&&959>screen_w?"tablet":300<screen_w&&767>screen_w?"mobile":300>screen_w?"mobile_portrait":""}function loadScript(url,$obj,callback){var script=document.createElement("script");script.type="text/javascript",script.readyState&&(script.onreadystatechange=function(){"loaded"!==script.readyState&&"complete"!==script.readyState||(script.onreadystatechange=null,callback())}),script.src=url,$obj.get(0).appendChild(script)}function vc_ttaActivation(){jQuery("[data-vc-accordion]").on("show.vc.accordion",function(e){var $=window.jQuery,ui={};ui.newPanel=$(this).data("vc.accordion").getTarget(),window.wpb_prepare_tab_content(e,ui)})}function vc_accordionActivate(event,ui){if(ui.newPanel.length&&ui.newHeader.length){var $pie_charts=ui.newPanel.find(".vc_pie_chart:not(.vc_ready)"),$round_charts=ui.newPanel.find(".vc_round-chart"),$line_charts=ui.newPanel.find(".vc_line-chart"),$carousel=ui.newPanel.find('[data-ride="vc_carousel"]');"undefined"!=typeof jQuery.fn.isotope&&ui.newPanel.find(".isotope, .wpb_image_grid_ul").isotope("layout"),ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),vc_carouselBehaviour(ui.newPanel),vc_plugin_flexslider(ui.newPanel),$pie_charts.length&&jQuery.fn.vcChat&&$pie_charts.vcChat(),$round_charts.length&&jQuery.fn.vcRoundChart&&$round_charts.vcRoundChart({reload:!1}),$line_charts.length&&jQuery.fn.vcLineChart&&$line_charts.vcLineChart({reload:!1}),$carousel.length&&jQuery.fn.carousel&&$carousel.carousel("resizeAction"),ui.newPanel.parents(".isotope").length&&ui.newPanel.parents(".isotope").each(function(){jQuery(this).isotope("layout")})}}function initVideoBackgrounds(){return window.console&&window.console.warn&&window.console.warn("this function is deprecated use vc_initVideoBackgrounds"),vc_initVideoBackgrounds()}function vc_initVideoBackgrounds(){jQuery("[data-vc-video-bg]").each(function(){var youtubeUrl,youtubeId,$element=jQuery(this);$element.data("vcVideoBg")?(youtubeUrl=$element.data("vcVideoBg"),youtubeId=vcExtractYoutubeId(youtubeUrl),youtubeId&&($element.find(".vc_video-bg").remove(),insertYoutubeVideoAsBackground($element,youtubeId)),jQuery(window).on("grid:items:added",function(event,$grid){$element.has($grid).length&&vcResizeVideoBackground($element)})):$element.find(".vc_video-bg").remove()})}function insertYoutubeVideoAsBackground($element,youtubeId,counter){if("undefined"==typeof YT||"undefined"==typeof YT.Player)return counter="undefined"==typeof counter?0:counter,100<counter?void console.warn("Too many attempts to load YouTube api"):void setTimeout(function(){insertYoutubeVideoAsBackground($element,youtubeId,counter++)},100);var $container=$element.prepend('<div class="vc_video-bg vc_hidden-xs"><div class="inner"></div></div>').find(".inner");new YT.Player($container[0],{width:"100%",height:"100%",videoId:youtubeId,playerVars:{playlist:youtubeId,iv_load_policy:3,enablejsapi:1,disablekb:1,autoplay:1,controls:0,showinfo:0,rel:0,loop:1,wmode:"transparent"},events:{onReady:function(event){event.target.mute().setLoop(!0)}}}),vcResizeVideoBackground($element),jQuery(window).bind("resize",function(){vcResizeVideoBackground($element)})}function vcResizeVideoBackground($element){var iframeW,iframeH,marginLeft,marginTop,containerW=$element.innerWidth(),containerH=$element.innerHeight(),ratio1=16,ratio2=9;containerW/containerH<ratio1/ratio2?(iframeW=containerH*(ratio1/ratio2),iframeH=containerH,marginLeft=-Math.round((iframeW-containerW)/2)+"px",marginTop=-Math.round((iframeH-containerH)/2)+"px",iframeW+="px",iframeH+="px"):(iframeW=containerW,iframeH=containerW*(ratio2/ratio1),marginTop=-Math.round((iframeH-containerH)/2)+"px",marginLeft=-Math.round((iframeW-containerW)/2)+"px",iframeW+="px",iframeH+="px"),$element.find(".vc_video-bg iframe").css({maxWidth:"1000%",marginLeft:marginLeft,marginTop:marginTop,width:iframeW,height:iframeH})}function vcExtractYoutubeId(url){if("undefined"==typeof url)return!1;var id=url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);return null!==id&&id[1]}function vc_googleMapsPointer(){var $=window.jQuery,$wpbGmapsWidget=$(".wpb_gmaps_widget");$wpbGmapsWidget.click(function(){$("iframe",this).css("pointer-events","auto")}),$wpbGmapsWidget.mouseleave(function(){$("iframe",this).css("pointer-events","none")}),$(".wpb_gmaps_widget iframe").css("pointer-events","none")}document.documentElement.className+=" js_active ",document.documentElement.className+="ontouchstart"in document.documentElement?" vc_mobile ":" vc_desktop ",function(){for(var prefix=["-webkit-","-moz-","-ms-","-o-",""],i=0;i<prefix.length;i++)prefix[i]+"transform"in document.documentElement.style&&(document.documentElement.className+=" vc_transform ")}(),"function"!=typeof window.vc_plugin_flexslider&&(window.vc_plugin_flexslider=function($parent){var $slider=$parent?$parent.find(".wpb_flexslider"):jQuery(".wpb_flexslider");$slider.each(function(){var this_element=jQuery(this),sliderSpeed=800,sliderTimeout=1e3*parseInt(this_element.attr("data-interval")),sliderFx=this_element.attr("data-flex_fx"),slideshow=!0;0===sliderTimeout&&(slideshow=!1),this_element.is(":visible")&&this_element.flexslider({animation:sliderFx,slideshow:slideshow,slideshowSpeed:sliderTimeout,sliderSpeed:sliderSpeed,smoothHeight:!0})})}),"function"!=typeof window.vc_googleplus&&(window.vc_googleplus=function(){0<jQuery(".wpb_googleplus").length&&!function(){var po=document.createElement("script");po.type="text/javascript",po.async=!0,po.src="//apis.google.com/js/plusone.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(po,s)}()}),"function"!=typeof window.vc_pinterest&&(window.vc_pinterest=function(){0<jQuery(".wpb_pinterest").length&&!function(){var po=document.createElement("script");po.type="text/javascript",po.async=!0,po.src="//assets.pinterest.com/js/pinit.js";var s=document.getElementsByTagName("script")[0];s.parentNode.insertBefore(po,s)}()}),"function"!=typeof window.vc_progress_bar&&(window.vc_progress_bar=function(){"undefined"!=typeof jQuery.fn.waypoint&&jQuery(".vc_progress_bar").waypoint(function(){jQuery(this).find(".vc_single_bar").each(function(index){var $this=jQuery(this),bar=$this.find(".vc_bar"),val=bar.data("percentage-value");setTimeout(function(){bar.css({width:val+"%"})},200*index)})},{offset:"85%"})}),"function"!=typeof window.vc_waypoints&&(window.vc_waypoints=function(){"undefined"!=typeof jQuery.fn.waypoint&&jQuery(".wpb_animate_when_almost_visible:not(.wpb_start_animation)").waypoint(function(){jQuery(this).addClass("wpb_start_animation animated")},{offset:"85%"})}),"function"!=typeof window.vc_toggleBehaviour&&(window.vc_toggleBehaviour=function($el){function event(e){e&&e.preventDefault&&e.preventDefault();var title=jQuery(this),element=title.closest(".vc_toggle"),content=element.find(".vc_toggle_content");element.hasClass("vc_toggle_active")?content.slideUp({duration:300,complete:function(){element.removeClass("vc_toggle_active")}}):content.slideDown({duration:300,complete:function(){element.addClass("vc_toggle_active")}})}$el?$el.hasClass("vc_toggle_title")?$el.unbind("click").click(event):$el.find(".vc_toggle_title").unbind("click").click(event):jQuery(".vc_toggle_title").unbind("click").on("click",event)}),"function"!=typeof window.vc_tabsBehaviour&&(window.vc_tabsBehaviour=function($tab){if(jQuery.ui){var $call=$tab||jQuery(".wpb_tabs, .wpb_tour"),ver=jQuery.ui&&jQuery.ui.version?jQuery.ui.version.split("."):"1.10",old_version=1===parseInt(ver[0])&&9>parseInt(ver[1]);$call.each(function(index){var $tabs,interval=jQuery(this).attr("data-interval"),tabs_array=[];if($tabs=jQuery(this).find(".wpb_tour_tabs_wrapper").tabs({show:function(event,ui){wpb_prepare_tab_content(event,ui)},beforeActivate:function(event,ui){1!==ui.newPanel.index()&&ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")},activate:function(event,ui){wpb_prepare_tab_content(event,ui)}}),interval&&0<interval)try{$tabs.tabs("rotate",1e3*interval)}catch(e){window.console&&window.console.log&&console.log(e)}jQuery(this).find(".wpb_tab").each(function(){tabs_array.push(this.id)}),jQuery(this).find(".wpb_tabs_nav li").click(function(e){return e.preventDefault(),old_version?$tabs.tabs("select",jQuery("a",this).attr("href")):$tabs.tabs("option","active",jQuery(this).index()),!1}),jQuery(this).find(".wpb_prev_slide a, .wpb_next_slide a").click(function(e){if(e.preventDefault(),old_version){var index=$tabs.tabs("option","selected");jQuery(this).parent().hasClass("wpb_next_slide")?index++:index--,0>index?index=$tabs.tabs("length")-1:index>=$tabs.tabs("length")&&(index=0),$tabs.tabs("select",index)}else{var index=$tabs.tabs("option","active"),length=$tabs.find(".wpb_tab").length;index=jQuery(this).parent().hasClass("wpb_next_slide")?index+1>=length?0:index+1:0>index-1?length-1:index-1,$tabs.tabs("option","active",index)}})})}}),"function"!=typeof window.vc_accordionBehaviour&&(window.vc_accordionBehaviour=function(){jQuery(".wpb_accordion").each(function(index){var $tabs,$this=jQuery(this),active_tab=($this.attr("data-interval"),!isNaN(jQuery(this).data("active-tab"))&&0<parseInt($this.data("active-tab"))&&parseInt($this.data("active-tab"))-1),collapsible=!1===active_tab||"yes"===$this.data("collapsible");$tabs=$this.find(".wpb_accordion_wrapper").accordion({header:"> div > h3",autoHeight:!1,heightStyle:"content",active:active_tab,collapsible:collapsible,navigation:!0,activate:vc_accordionActivate,change:function(event,ui){"undefined"!=typeof jQuery.fn.isotope&&ui.newContent.find(".isotope").isotope("layout"),vc_carouselBehaviour(ui.newPanel)}}),!0===$this.data("vcDisableKeydown")&&($tabs.data("uiAccordion")._keydown=function(){})})}),"function"!=typeof window.vc_teaserGrid&&(window.vc_teaserGrid=function(){var layout_modes={fitrows:"fitRows",masonry:"masonry"};jQuery(".wpb_grid .teaser_grid_container:not(.wpb_carousel), .wpb_filtered_grid .teaser_grid_container:not(.wpb_carousel)").each(function(){var $container=jQuery(this),$thumbs=$container.find(".wpb_thumbnails"),layout_mode=$thumbs.attr("data-layout-mode");$thumbs.isotope({itemSelector:".isotope-item",layoutMode:"undefined"==typeof layout_modes[layout_mode]?"fitRows":layout_modes[layout_mode]}),$container.find(".categories_filter a").data("isotope",$thumbs).click(function(e){e.preventDefault();var $thumbs=jQuery(this).data("isotope");jQuery(this).parent().parent().find(".active").removeClass("active"),jQuery(this).parent().addClass("active"),$thumbs.isotope({filter:jQuery(this).attr("data-filter")})}),jQuery(window).bind("load resize",function(){$thumbs.isotope("layout")})})}),"function"!=typeof window.vc_carouselBehaviour&&(window.vc_carouselBehaviour=function($parent){var $carousel=$parent?$parent.find(".wpb_carousel"):jQuery(".wpb_carousel");$carousel.each(function(){var $this=jQuery(this);if(!0!==$this.data("carousel_enabled")&&$this.is(":visible")){$this.data("carousel_enabled",!0);var carousel_speed=(getColumnsCount(jQuery(this)),500);jQuery(this).hasClass("columns_count_1")&&(carousel_speed=900);var carousele_li=jQuery(this).find(".wpb_thumbnails-fluid li");carousele_li.css({"margin-right":carousele_li.css("margin-left"),"margin-left":0});var fluid_ul=jQuery(this).find("ul.wpb_thumbnails-fluid");fluid_ul.width(fluid_ul.width()+300),jQuery(window).resize(function(){var before_resize=screen_size;screen_size=getSizeName(),before_resize!=screen_size&&window.setTimeout("location.reload()",20)})}})}),"function"!=typeof window.vc_slidersBehaviour&&(window.vc_slidersBehaviour=function(){jQuery(".wpb_gallery_slides").each(function(index){var $imagesGrid,this_element=jQuery(this);if(this_element.hasClass("wpb_slider_nivo")){var sliderSpeed=800,sliderTimeout=1e3*this_element.attr("data-interval");0===sliderTimeout&&(sliderTimeout=9999999999),this_element.find(".nivoSlider").nivoSlider({effect:"boxRainGrow,boxRain,boxRainReverse,boxRainGrowReverse",slices:15,boxCols:8,boxRows:4,animSpeed:sliderSpeed,pauseTime:sliderTimeout,startSlide:0,directionNav:!0,directionNavHide:!0,controlNav:!0,keyboardNav:!1,pauseOnHover:!0,manualAdvance:!1,prevText:"Prev",nextText:"Next"})}else this_element.hasClass("wpb_image_grid")&&(jQuery.fn.imagesLoaded?$imagesGrid=this_element.find(".wpb_image_grid_ul").imagesLoaded(function(){$imagesGrid.isotope({itemSelector:".isotope-item",layoutMode:"fitRows"})}):this_element.find(".wpb_image_grid_ul").isotope({itemSelector:".isotope-item",layoutMode:"fitRows"}))})}),"function"!=typeof window.vc_prettyPhoto&&(window.vc_prettyPhoto=function(){try{jQuery&&jQuery.fn&&jQuery.fn.prettyPhoto&&jQuery('a.prettyphoto, .gallery-icon a[href*=".jpg"]').prettyPhoto({animationSpeed:"normal",hook:"data-rel",padding:15,opacity:.7,showTitle:!0,allowresize:!0,counter_separator_label:"/",hideflash:!1,deeplinking:!1,modal:!1,callback:function(){var url=location.href;url.indexOf("#!prettyPhoto")>-1&&(location.hash="")},social_tools:""})}catch(err){window.console&&window.console.log&&console.log(err)}}),"function"!=typeof window.vc_google_fonts&&(window.vc_google_fonts=function(){return!1}),window.vcParallaxSkroll=!1,"function"!=typeof window.vc_rowBehaviour&&(window.vc_rowBehaviour=function(){function fullWidthRow(){var $elements=$('[data-vc-full-width="true"]');$.each($elements,function(key,item){var $el=$(this);$el.addClass("vc_hidden");var $el_full=$el.next(".vc_row-full-width");if($el_full.length||($el_full=$el.parent().next(".vc_row-full-width")),$el_full.length){var el_margin_left=parseInt($el.css("margin-left"),10),el_margin_right=parseInt($el.css("margin-right"),10),offset=0-$el_full.offset().left-el_margin_left,width=$(window).width();if($el.css({position:"relative",left:offset,"box-sizing":"border-box",width:$(window).width()}),!$el.data("vcStretchContent")){var padding=-1*offset;0>padding&&(padding=0);var paddingRight=width-padding-$el_full.width()+el_margin_left+el_margin_right;0>paddingRight&&(paddingRight=0),$el.css({"padding-left":padding+"px","padding-right":paddingRight+"px"})}$el.attr("data-vc-full-width-init","true"),$el.removeClass("vc_hidden"),$(document).trigger("vc-full-width-row-single",{el:$el,offset:offset,marginLeft:el_margin_left,marginRight:el_margin_right,elFull:$el_full,width:width})}}),$(document).trigger("vc-full-width-row",$elements)}function parallaxRow(){var vcSkrollrOptions,callSkrollInit=!1;return window.vcParallaxSkroll&&window.vcParallaxSkroll.destroy(),$(".vc_parallax-inner").remove(),$("[data-5p-top-bottom]").removeAttr("data-5p-top-bottom data-30p-top-bottom"),$("[data-vc-parallax]").each(function(){var skrollrSpeed,skrollrSize,skrollrStart,skrollrEnd,$parallaxElement,parallaxImage,youtubeId;callSkrollInit=!0,"on"===$(this).data("vcParallaxOFade")&&$(this).children().attr("data-5p-top-bottom","opacity:0;").attr("data-30p-top-bottom","opacity:1;"),skrollrSize=100*$(this).data("vcParallax"),$parallaxElement=$("<div />").addClass("vc_parallax-inner").appendTo($(this)),$parallaxElement.height(skrollrSize+"%"),parallaxImage=$(this).data("vcParallaxImage"),youtubeId=vcExtractYoutubeId(parallaxImage),youtubeId?insertYoutubeVideoAsBackground($parallaxElement,youtubeId):"undefined"!=typeof parallaxImage&&$parallaxElement.css("background-image","url("+parallaxImage+")"),skrollrSpeed=skrollrSize-100,skrollrStart=-skrollrSpeed,skrollrEnd=0,$parallaxElement.attr("data-bottom-top","top: "+skrollrStart+"%;").attr("data-top-bottom","top: "+skrollrEnd+"%;")}),!(!callSkrollInit||!window.skrollr)&&(vcSkrollrOptions={forceHeight:!1,smoothScrolling:!1,mobileCheck:function(){return!1}},window.vcParallaxSkroll=skrollr.init(vcSkrollrOptions),window.vcParallaxSkroll)}function fullHeightRow(){var $element=$(".vc_row-o-full-height:first");if($element.length){var $window,windowHeight,offsetTop,fullHeight;$window=$(window),windowHeight=$window.height(),offsetTop=$element.offset().top,offsetTop<windowHeight&&(fullHeight=100-offsetTop/(windowHeight/100),$element.css("min-height",fullHeight+"vh"))}$(document).trigger("vc-full-height-row",$element)}function fixIeFlexbox(){var ua=window.navigator.userAgent,msie=ua.indexOf("MSIE ");(msie>0||navigator.userAgent.match(/Trident.*rv\:11\./))&&$(".vc_row-o-full-height").each(function(){"flex"===$(this).css("display")&&$(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')})}var $=window.jQuery;$(window).off("resize.vcRowBehaviour").on("resize.vcRowBehaviour",fullWidthRow).on("resize.vcRowBehaviour",fullHeightRow),fullWidthRow(),fullHeightRow(),fixIeFlexbox(),vc_initVideoBackgrounds(),parallaxRow()}),"function"!=typeof window.vc_gridBehaviour&&(window.vc_gridBehaviour=function(){jQuery.fn.vcGrid&&jQuery("[data-vc-grid]").vcGrid()}),"function"!=typeof window.getColumnsCount&&(window.getColumnsCount=function(el){for(var find=!1,i=1;!1===find;){if(el.hasClass("columns_count_"+i))return find=!0,i;i++}});var screen_size=getSizeName();"function"!=typeof window.wpb_prepare_tab_content&&(window.wpb_prepare_tab_content=function(event,ui){var $ui_panel,$google_maps,panel=ui.panel||ui.newPanel,$pie_charts=panel.find(".vc_pie_chart:not(.vc_ready)"),$round_charts=panel.find(".vc_round-chart"),$line_charts=panel.find(".vc_line-chart"),$carousel=panel.find('[data-ride="vc_carousel"]');if(vc_carouselBehaviour(),vc_plugin_flexslider(panel),ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),panel.find(".vc_masonry_media_grid, .vc_masonry_grid").length&&panel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function(){var grid=jQuery(this).data("vcGrid");grid&&grid.gridBuilder&&grid.gridBuilder.setMasonry&&grid.gridBuilder.setMasonry()}),$pie_charts.length&&jQuery.fn.vcChat&&$pie_charts.vcChat(),$round_charts.length&&jQuery.fn.vcRoundChart&&$round_charts.vcRoundChart({reload:!1}),$line_charts.length&&jQuery.fn.vcLineChart&&$line_charts.vcLineChart({reload:!1}),$carousel.length&&jQuery.fn.carousel&&$carousel.carousel("resizeAction"),$ui_panel=panel.find(".isotope, .wpb_image_grid_ul"),$google_maps=panel.find(".wpb_gmaps_widget"),0<$ui_panel.length&&$ui_panel.isotope("layout"),$google_maps.length&&!$google_maps.is(".map_ready")){var $frame=$google_maps.find("iframe");$frame.attr("src",$frame.attr("src")),$google_maps.addClass("map_ready")}panel.parents(".isotope").length&&panel.parents(".isotope").each(function(){jQuery(this).isotope("layout")})}),"function"!=typeof window.vc_googleMapsPointer,jQuery(document).ready(function($){window.vc_js()});
</script>
<script>
    $(function(){ vc_js(); });
</script>
<script>
    /* global wq_l10n, alert */
    /*
     * WP Quiz Pro Plugin by MyThemeShop
     * https://mythemeshop.com/plugins/wp-quiz-pro/
    */

    ;( function( $ ) {

        'use strict';

        // Move or create fb-root
        function moveFbRoot() {
            var fbRoot = $( '#fb-root' );
            if ( fbRoot.length > 0 ) {
                $( 'body' ).prepend( fbRoot );
            } else {
                $( 'body' ).prepend( '<div id="fb-root"/>' );
            }
        }

        moveFbRoot();

        if ( 'function' === typeof $.fn.flip ) {
            $( '.wq_IsFlip .card' ).flip();
        }

        if ( 'function' === typeof $.fn.jTinder ) {
            $( '.wq_IsSwiper' ).jTinder({
                infinite: false,
                onDislike: function( item, element ) {

                    var id = $( item ).data( 'slideid' ),
                        resItem		= $( element ).closest( '.wq_quizCtr' ).find( '.wq_singleResultWrapper' ).find( '#result-' + id );

                    resItem.data( 'result', 0 );
                    resItem.find( '.resultContent' ).append( '<div class="userVote negativeVote">' + wq_l10n.youVoted + ' <i class="sprite sprite-times"></i></div>' );
                    var count = resItem.find( '#downVote' ).text();
                    if ( -1 === count.indexOf( 'K' ) ) {
                        var newCount = parseInt( count ) + 1;
                        resItem.find( '#downVote' ).text( newCount );
                    }
                },
                onLike: function( item, element ) {

                    var id = $( item ).data( 'slideid' ),
                        resItem		= $( element ).closest( '.wq_quizCtr' ).find( '.wq_singleResultWrapper' ).find( '#result-' + id );

                    resItem.data( 'result', 1 );
                    resItem.find( '.resultContent' ).append( '<div class="userVote positiveVote">' + wq_l10n.youVoted + ' <i class="sprite sprite-check"></i></div>' );
                    var count = resItem.find( '#upVote' ).text();
                    if ( -1 === count.indexOf( 'K' ) ) {
                        var newCount = parseInt( count ) + 1;
                        resItem.find( '#upVote' ).text( newCount );
                    }
                },
                onEndStack: function( instance, element ) {

                    var quizElem		= $( element ).closest( '.wq_quizCtr' ),
                        isRetakeable	= parseInt( quizElem.data( 'retake-quiz' ) ),
                        allQElem		= quizElem.find( '.wq_questionsCtr' ),
                        resElem			= quizElem.find( '.wq_singleResultWrapper' ),
                        resultsObj		= {};

                    resElem.find( '.resultItem' ).each(function() {

                        var $this = $( this );

                        resultsObj[ $this.data( 'uid' ) ] = $this.data( 'result' );
                    });

                    $.post( quizElem.data( 'ajax-url' ), {
                        action: 'wq_quizResults',
                        pid: quizElem.data( 'quiz-pid' ),
                        results: resultsObj,
                        type: 'swiper',
                        _nonce: wq_l10n.nonce
                    });

                    allQElem.fadeOut( 'slow' );

                    setTimeout(function() {

                        resElem.fadeIn( 'slow' );

                        if ( isRetakeable ) {
                            quizElem.find( '.wq_retakeSwiperBtn' ).show();
                        }
                    }, 900 );

                    $( '.wq_retakeSwiperBtn' ).on( 'click', function() {

                        instance.restart();
                        resElem.find( '.userVote' ).remove();
                        resElem.fadeOut( 'slow' );

                        setTimeout(function() {
                            quizElem.find( '.wq_retakeSwiperBtn' ).hide();
                            allQElem.fadeIn( 'slow' );
                        }, 900 );
                    });
                }
            });

            $( '.actions .like, .actions .dislike' ).click(function( event ) {

                event.preventDefault();

                $( '.wq_IsSwiper' ).jTinder( $( this ).attr( 'class' ) );
            });
        }

        $( document ).on( 'click', '.wq_beginQuizSwiperCtr', function( event ) {

            event.preventDefault();

            var quizElem = $( this ).closest( '.wq_quizCtr' );
            quizElem.find( '.wq_swiperQuizPreviewInfoCtr' ).fadeOut( 'slow' );
            quizElem.find( '.wq_QuestionWrapperSwiper' ).fadeIn( 'slow' );
        });

        $( document ).on( 'click', '.wq_btn-continue', function( event ) {

            event.preventDefault();

            var quizElem			= $( this ).closest( '.wq_quizCtr' ),
                curQ				= parseInt( quizElem.data( 'current-question' ) ),
                curQElem			= quizElem.find( '.wq_questionsCtr > .wq_singleQuestionWrapper' ).eq( curQ ),
                totalQuestionsNum	= parseInt( quizElem.data( 'questions' ) ),
                questionsAnswered	= parseInt( quizElem.data( 'questions-answered' ) ),
                questionPercent		= parseInt( ( questionsAnswered / totalQuestionsNum ) * 100 ),
                quizTimer			= parseInt( quizElem.data( 'quiz-timer' ) );

            if ( ! curQElem.next().length ) {
                return;
            }



            curQElem.transition({
                animation: quizElem.data( 'transition_out' ),
                onComplete: function() {

                    var next = curQElem.next();

                    curQElem.hide();

                    next.transition({
                        animation: quizElem.data( 'transition_in' ),
                        onComplete: function() {

                            if ( next.hasClass( 'wq_isAd' ) ) {

                                var template = next.find( 'template' );

                                if ( template.length > 0 ) {
                                    var ad = template.html();
                                    template.replaceWith( ad );
                                }
                            }
                        }
                    });

                    quizElem.data( 'current-question', curQ + 1 );
                }
            });

            quizElem.find( '.wq_quizProgressValue' ).animate({
                width: questionPercent + '%'
            }).text( questionPercent + '%' );

            quizElem.find( '.wq_triviaQuizTimerCtr' ).text( quizTimer );

            $( 'html, body' ).animate({
                scrollTop: quizElem.offset().top - 35
            }, 750 );
        });

        $( document ).on( 'submit', '#wq_infoForm', function( event ) {

            event.preventDefault();

            var form			= $( this ),
                username		= form.find( '#wq_inputName' ).val(),
                email			= form.find( '#wq_inputEmail' ).val(),
                quizElem		= form.closest( '.wq_quizCtr' ),
                ajaxurl			= quizElem.data( 'ajax-url' ),
                isRetakeable	= parseInt( quizElem.data( 'retake-quiz' ) );

            if ( '' === username || '' === email ) {
                alert( 'Please enter your name and email address !' );
                return;
            }

            var formObj = {
                action: 'wq_submitInfo',
                username: username,
                email: email,
                pid: quizElem.data( 'quiz-pid' ),
                _nonce: wq_l10n.nonce
            };

            $.post( ajaxurl, formObj, function( response ) {
                if ( 2 === parseInt( response.status ) ) {

                    var animationIn		= quizElem.data( 'transition_in' ),
                        animationOut	= quizElem.data( 'transition_out' );

                    quizElem.find( '.wq_quizForceActionCtr' ).transition({
                        animation: animationOut,
                        onComplete: function() {
                            quizElem.find( '.wq_resultsCtr' ).transition({
                                animation: animationIn
                            });
                        }
                    });

                    if ( isRetakeable ) {
                        quizElem.find( '.wq_retakeQuizCtr' ).transition({ animation: animationIn });
                    }
                } else {
                    alert( 'Error submiting details, please try again' );
                }
            });
        });

        $( document ).on( 'click', '.wq_retakeQuizBtn', function( event ) {

            event.preventDefault();

            var quizElem		= $( this ).closest( '.wq_quizCtr' ),
                animationIn		= quizElem.data( 'transition_in' ),
                animationOut	= quizElem.data( 'transition_out' ),
                qTime			= parseInt( quizElem.data( 'quiz-timer' ) ),
                questionLayout	= quizElem.data( 'question-layout' );

            // Reset Quiz
            quizElem.data( 'current-question', 0 );
            quizElem.data( 'questions-answered', 0 );
            quizElem.data( 'correct-answered', 0 );
            quizElem.find( '.wq_quizProgressValue' ).animate({
                width: '0%'
            }).text( '' );

            // Reset All Questions
            quizElem.find( '.wq_questionsCtr > .wq_singleQuestionWrapper' ).each(function() {

                var $this = $( this );

                $this.find( '.wq_triviaQuestionExplanation' ).removeClass( 'transition visible' );
                $this.find( '.wq_triviaQuestionExplanation' ).hide();
                $this.find( '.wq_singleAnswerCtr' ).removeClass( 'wq_incorrectAnswer wq_correctAnswer chosen wq_answerSelected' );
                $this.find( '.wq_ExplanationHead' ).removeClass( 'wq_correctExplanationHead wq_wrongExplanationHead' );
                $this.data( 'question-answered', 1 );
                $this.removeClass( 'wq_questionAnswered' );
            });

            // Reset Results
            quizElem.find( '.wq_singleResultWrapper, .wq_singleResultRow' ).data( 'points', 0 );

            // Hide results and show first question
            quizElem.find( '.wq_questionsCtr' ).show();
            quizElem.find( '.wq_singleResultWrapper.transition.visible' ).transition({
                animation: animationOut,
                onComplete: function() {
                    quizElem.find( '.wq_singleResultWrapper' ).hide();
                    if ( 'multiple' === questionLayout ) {
                        if ( qTime > 0 ) {
                            quizElem.find( '.wq_questionsCtr' ).hide();
                            quizElem.find( '.wq_questionsCtr > .wq_singleQuestionWrapper:eq(0)' ).show();
                            quizElem.find( '.wq_triviaQuizTimerCtr' ).text( qTime );
                            quizElem.find( '.wq_triviaQuizTimerInfoCtr' ).transition({
                                animation: animationIn
                            });
                        } else {
                            quizElem.find( '.wq_questionsCtr > .wq_singleQuestionWrapper:last' ).transition({
                                animation: animationOut,
                                onComplete: function() {
                                    quizElem.find( '.wq_questionsCtr > .wq_singleQuestionWrapper:eq(0)' ).transition({
                                        animation: animationIn
                                    });

                                }
                            });
                        }
                    }

                    // Reset Results
                    quizElem.find( '.wq_singleResultWrapper, .wq_resultsTable, .wq_shareResultCtr, .wq_resultsCtr, .wq_quizForceActionCtr, .wq_quizEmailCtr, .wq_quizForceShareCtr, .wq_retakeQuizBtn, .wq_retakeQuizCtr' ).removeClass( 'transition hidden visible' );
                    quizElem.find( '.wq_resultExplanation, .wq_quizForceActionCtr, .wq_quizEmailCtr, .wq_quizForceShareCtr, .wq_retakeQuizBtn' ).hide();
                }
            });

            $( 'html, body' ).animate({
                scrollTop: quizElem.offset().top - 35
            }, 750 );

            $( this ).removeClass( 'transition visible' ).hide();
        });

        // Embed toggle
        $( document ).on( 'click', '.wq_embedToggleQuizCtr a', function( event ) {
            event.preventDefault();

            $( this ).parent().toggleClass( 'active' ).next().slideToggle( 'fast' );
        });

        // Share - Facebook
        $( document ).on( 'click', '.wq_forceShareFB', function( event ) {

            event.preventDefault();

            var quizElem	= $( this ).closest( '.wq_quizCtr' ),
                shareURL	= quizElem.data( 'share-url' ) ? quizElem.data( 'share-url' ) : document.referrer;

            var base64 = {
                id: parseInt( quizElem.data( 'quiz-pid' ) ),
                pic: 'f',
                desc: 'e'
            };

            base64 = $.param( base64 );

            FB.ui({
                method: 'share',
                href: shareURL + '?fbs=1&' + base64
            }, function( response ) {

                if ( 'undefined' === typeof response ) {
                    return;
                }

                quizElem.find( '.wq_quizForceActionCtr' ).transition({
                    animation: quizElem.data( 'transition_out' ),
                    onComplete: function() {
                        quizElem.find( '.wq_resultsCtr' ).transition({
                            animation: quizElem.data( 'transition_in' )
                        });
                    }
                });
            });
        });

        $( document ).on( 'click', '.wq_shareFB', function( event ) {

            event.preventDefault();

            var quizElem	= $( this ).closest( '.wq_quizCtr' ),
                shareURL	= quizElem.data( 'share-url' ) ? quizElem.data( 'share-url' ) : document.referrer,
                resultElem	= quizElem.find( '.wq_singleResultWrapper:visible' ),
                description	= resultElem.find( '.wq_resultDesc' ).text(),
                picture		= resultElem.find( '.wq_resultImg' ).attr( 'src' ),
                shareText;

            if ( resultElem.hasClass( 'wq_IsTrivia' ) ) {
                var correctAnswered		= parseInt( quizElem.data( 'correct-answered' ) ),
                    totalQuestionsNum	= parseInt( quizElem.data( 'questions' ) );

                shareText = wq_l10n.captionTriviaFB.replace( '%%score%%', correctAnswered ).replace( '%%total%%', totalQuestionsNum );

            } else if ( resultElem.hasClass( 'wq_IsPersonality' ) ) {
                shareText = resultElem.find( '.wq_resultTitle' ).data( 'title' );
            } else {
                shareText = quizElem.data( 'post-title' );
            }

            var base64 = {
                id: parseInt( quizElem.data( 'quiz-pid' ) ),
                rid: parseInt( resultElem.data( 'id' ) ),
                pic: picture ? 'r' : 'f',
                text: shareText,
                desc: description ? 'r' : 'e',
                ts: Date.now()
            };

            if ( quizElem.hasClass( 'fb_quiz_quiz' ) ) {
                base64.pic = picture.split( '/' ).pop().replace( '.png', '' );
                base64.nf = quizElem.data( 'user-info' ).first_name;
                base64.nl = quizElem.data( 'user-info' ).last_name;
            }

            base64 = $.param( base64 );

            FB.ui({
                method: 'share',
                href: shareURL + '?fbs=1&' + base64
            }, function() {});
        });

        // Share - Twitter
        $( document ).on( 'click', '.wq_shareTwitter', function( event ) {
            event.preventDefault();

            var quizElem	= $( this ).closest( '.wq_quizCtr' ),
                shareURL	= quizElem.data( 'share-url' ) ? quizElem.data( 'share-url' ) : document.referrer,
                resultElem	= quizElem.find( '.wq_singleResultWrapper:visible' ),
                description	= resultElem.find( '.wq_resultDesc' ).text(),
                picture		= resultElem.find( '.wq_resultImg' ).attr( 'src' ),
                shareText = '';

            if ( resultElem.hasClass( 'wq_IsTrivia' ) ) {
                var correctAnswered		= parseInt( quizElem.data( 'correct-answered' ) ),
                    totalQuestionsNum	= parseInt( quizElem.data( 'questions' ) );

                shareText = wq_l10n.captionTriviaFB.replace( '%%score%%', correctAnswered ).replace( '%%total%%', totalQuestionsNum );
            } else if ( resultElem.hasClass( 'wq_IsPersonality' ) ) {
                shareText = resultElem.find( '.wq_resultTitle' ).data( 'title' );
            } else {
                shareText = quizElem.data( 'post-title' );
            }

            var base64 = {
                id: parseInt( quizElem.data( 'quiz-pid' ) ),
                rid: parseInt( resultElem.data( 'id' ) ),
                pic: picture ? 'r' : 'f',
                desc: description ? 'r' : 'e',
                text: shareText
            };

            base64 = $.param( base64 );

            window.open(
                'https://twitter.com/share?url=' + encodeURIComponent( shareURL + '?fbs=1&' + base64 ),
                '_blank',
                'width=500, height=300'
            );
        });

        // Share - Google+
        $( document ).on( 'click', '.wq_shareGP', function( event ) {

            event.preventDefault();

            var quizElem	= $( this ).closest( '.wq_quizCtr' ),
                shareURL	= quizElem.data( 'share-url' ) ? quizElem.data( 'share-url' ) : document.referrer,
                resultElem	= quizElem.find( '.wq_singleResultWrapper:visible' ),
                description	= resultElem.find( '.wq_resultDesc' ).text(),
                picture		= resultElem.find( '.wq_resultImg' ).attr( 'src' ),
                shareText = '';

            if ( resultElem.hasClass( 'wq_IsTrivia' ) ) {
                var correctAnswered		= parseInt( quizElem.data( 'correct-answered' ) ),
                    totalQuestionsNum	= parseInt( quizElem.data( 'questions' ) );

                shareText = wq_l10n.captionTriviaFB.replace( '%%score%%', correctAnswered ).replace( '%%total%%', totalQuestionsNum );
            } else if ( resultElem.hasClass( 'wq_IsPersonality' ) ) {
                shareText = resultElem.find( '.wq_resultTitle' ).data( 'title' );
            } else {
                shareText = quizElem.data( 'post-title' );
            }

            var base64 = {
                id: parseInt( quizElem.data( 'quiz-pid' ) ),
                rid: parseInt( resultElem.data( 'id' ) ),
                pic: picture ? 'r' : 'f',
                desc: description ? 'r' : 'e',
                text: shareText
            };

            base64 = $.param( base64 );

            window.open(
                'https://plus.google.com/share?url=' + encodeURIComponent( shareURL + '?fbs=1&' + base64 ),
                '_blank',
                'width=500, height=300'
            );
        });

        // Share - VK
        $( document ).on( 'click', '.wq_shareVK', function( event ) {

            event.preventDefault();

            var quizElem = $( this ).closest( '.wq_quizCtr' ),
                shareURL = quizElem.data( 'share-url' ) ? quizElem.data( 'share-url' ) : document.referrer;

            window.open(
                'http://vk.com/share.php?url=' + shareURL,
                '_blank',
                'width=500, height=300'
            );
        });

        // Document Ready
        $( document ).ready(function() {

            function flipDescResizeAndReady( event ) {

                $( '.wq_IsFlip .back' ).each(function() {

                    var $this = $( this ),
                        bImg = $this.find( 'img' ).attr( 'src' ),
                        titleH = $( this ).siblings( '.item_top' ).height();

                    $this.css( 'top', titleH + 'px' );

                    if ( '' === bImg ) {

                        $this.siblings( '.front' ).find( 'img' ).on( 'load', function() {
                            $this.find( '.desc' ).height( $( this ).height() );
                        });

                        if ( 'resize' === event.type ) {
                            var imgHeight = $this.siblings( '.front' ).find( 'img' ).height();
                            $this.find( '.desc' ).height( imgHeight );
                        }
                    }
                });
            }

            $( document ).on( 'ready', flipDescResizeAndReady );
            $( window ).on( 'resize', flipDescResizeAndReady );
            $( window ).trigger( 'resize' );
        });

        // Trivia
        function processResults( quizElem ) {

            var forceAction			= parseInt( quizElem.data( 'force-action' ) ),
                animationIn			= quizElem.data( 'transition_in' ),
                correctAnswered		= parseInt( quizElem.data( 'correct-answered' ) ),
                totalQuestionsNum	= parseInt( quizElem.data( 'questions' ) ),
                endAnswers			= quizElem.data( 'end-answers' ),
                isRetakeable		= parseInt( quizElem.data( 'retake-quiz' ) );

            quizElem.find( '.wq_triviaQuizTimerCtr, .wq_continue' ).hide();

            if (totalQuestionsNum.length -1)

                if ( endAnswers ) {

                    quizElem.find( '.wq_singleAnswerCtr.wq_IsTrivia' ).each(function() {

                        var $answer = $( this );

                        if ( $answer.hasClass( 'wq_correctEndAnswer' ) ) {
                            $answer.removeClass( 'wq_correctEndAnswer' ).addClass( 'wq_correctAnswer' );
                        } else if ( $answer.hasClass( 'wq_incorrectEndAnswer' ) ) {
                            $answer.removeClass( 'wq_incorrectEndAnswer' ).addClass( 'wq_incorrectAnswer' );
                        }
                    });


                    quizElem.find( '.wq_triviaQuestionExplanation' ).show( 'slow' );
                    $( 'html, body' ).animate({
                        scrollTop: quizElem.find( '.wq_singleQuestionWrapper:first-of-type' ).offset().top - 95
                    }, 750 );
                }

            if ( forceAction > 0 ) {

                var selector = (function() {

                    if ( 1 === forceAction ) {
                        return '.wq_quizEmailCtr';
                    }

                    if ( 2 === forceAction ) {
                        return '.wq_quizForceShareCtr';
                    }

                })();

                quizElem.find( '.wq_quizForceActionCtr' ).transition({
                    animation: animationIn,
                    onComplete: function() {
                        quizElem.find( selector ).transition({
                            animation: animationIn
                        });
                    }
                });

                quizElem.find( '.wq_resultsCtr, .wq_retakeQuizCtr' ).hide();

            }

            var resultFound = false;

            quizElem.find( '.wq_singleResultWrapper' ).each(function() {
                var $result = $( this ),
                    min		= parseInt( $result.data( 'min' ) ),
                    max		= parseInt( $result.data( 'max' ) );

                if ( correctAnswered >= min && correctAnswered <= max && ! resultFound ) {
                    resultFound = true;
                    var title = wq_l10n.captionTrivia.replace( '%%score%%', correctAnswered ).replace( '%%total%%', totalQuestionsNum );
                    $result.find( '.wq_resultScoreCtr' ).text( title );
                    $result.transition({ animation: animationIn });
                    return;
                }
            });

            if ( isRetakeable ) {
                quizElem.find( '.wq_retakeQuizBtn' ).transition({ animation: animationIn });
            }

            $.post( 'quiz/correct', {
                correct: correctAnswered,
                _token: "{{ csrf_token() }}",
            });
        }

        $( document ).on( 'click', '.wq_singleQuestionWrapper:not(.wq_questionAnswered) .wq_singleAnswerCtr.wq_IsTrivia', function( event ) {
            event.preventDefault();

            var $this				= $( this ),
                isCorrect			= parseInt( $this.data( 'crt' ) ),
                questionElem		= $this.closest( '.wq_singleQuestionWrapper' ),
                quizElem			= $this.closest( '.wq_quizCtr' ),
                current_user        = parseInt( quizElem.data( 'current-user' ) ),
                totalQuestionsNum	= parseInt( quizElem.data( 'questions' ) ),
                questionsAnswered	= parseInt( quizElem.data( 'questions-answered' ) ) + 1,
                correctAnswered		= parseInt( quizElem.data( 'correct-answered' ) ),
                curQ				= parseInt( quizElem.data( 'current-question' ) ),
                questionLayout		= quizElem.data( 'question-layout' ),
                endAnswers			= quizElem.data( 'end-answers' );

            questionElem.addClass( 'wq_questionAnswered' );



            // Process Correct Answer
            var correctClass	= endAnswers ? 'wq_correctEndAnswer' : 'wq_correctAnswer',
                incorrectClass	= endAnswers ? 'wq_incorrectEndAnswer' : 'wq_incorrectAnswer';

            if ( 1 === isCorrect  ) {

                correctAnswered++;
                $this.addClass( correctClass + ' chosen' );
                questionElem.find( '.wq_triviaQuestionExplanation .wq_ExplanationHead' ).text( wq_l10n.correct ).addClass( 'wq_correctExplanationHead' );
                quizElem.data( 'correct-answered', correctAnswered );

            } else {
                questionElem.find( '.wq_singleAnswerCtr' ).each(function() {
                    if ( 1 === $( this ).data( 'crt' ) ) {
                        $( this ).addClass( correctClass );
                    }
                });
                $this.addClass( incorrectClass + ' chosen' );
                questionElem.find( '.wq_triviaQuestionExplanation .wq_ExplanationHead' ).text( wq_l10n.wrong ).addClass( 'wq_wrongExplanationHead' );
            }

            if ( 'single' === questionLayout ) {
                curQ = parseInt( quizElem.data( 'current-question' ) );
                quizElem.data( 'current-question', curQ + 1 );
            } else {
                questionElem.find( '.wq_continue' ).show();
                questionElem.find('.chosen').parent().css('background-color', 'red');
            }

            quizElem.data( 'questions-answered', questionsAnswered );

            if ( ! endAnswers ) {
                questionElem.find( '.wq_triviaQuestionExplanation' ).show();
            }

            if ( 1 === parseInt( quizElem.data( 'auto-scroll' ) ) ) {
                var nextScroll = questionElem.next().length ? questionElem.next().offset() : quizElem.find( '.wq_resultsCtr' ).offset();

            }

            if ( totalQuestionsNum === questionsAnswered ) {

                quizElem.find( '.wq_quizProgressValue' ).animate({ width: '100%' }).text( '100%' );
                $this.find( 'wq_questionAnswered').hide();
                clearInterval( window.timerInterval );
                processResults( quizElem );
                return;
            }
        });

        $( document ).on( 'click', '.wq_beginQuizCtr', function( event ) {

            event.preventDefault();

            var quizElem		= $( this ).closest( '.wq_quizCtr' ),
                animationIn		= quizElem.data( 'transition_in' ),
                animationOut	= quizElem.data( 'transition_out' ),
                questionCtr		= quizElem.find( '.wq_questionsCtr' );

            quizElem.find( '.wq_triviaQuizTimerCtr' ).show();
            quizElem.find( '.wq_triviaQuizTimerInfoCtr' ).transition({
                animation: animationOut,
                onComplete: function() {
                    questionCtr.transition({
                        animation: animationIn,
                        onComplete: function() {

                            questionCtr.removeClass( 'visible' );
                            questionCtr.attr( 'style', 'display:block;' );

                            window.timerInterval = setInterval(function() {

                                var curSec		= parseInt( quizElem.find( '.wq_triviaQuizTimerCtr' ).text() ) - 1,
                                    quizTimer	= parseInt( quizElem.data( 'quiz-timer' ) );

                                if ( 0 === curSec ) {
                                    var curQ		= parseInt( quizElem.data( 'current-question' ) ),
                                        curQElem	= $( '.wq_questionsCtr > .wq_singleQuestionWrapper' ).eq( curQ );

                                    if ( ! curQElem.next().length ) {

                                        clearInterval( window.timerInterval );
                                        processResults( quizElem );
                                        return;
                                    }

                                    curQElem.find( '.wq_btn-continue' ).trigger( 'click' );
                                    quizElem.find( '.wq_triviaQuizTimerCtr' ).text( quizTimer );
                                    return;
                                }

                                quizElem.find( '.wq_triviaQuizTimerCtr' ).text( curSec );

                            }, 1000 );
                        }
                    });
                }
            });
        });

        // Personality
        $( document ).on( 'click', '.wq_singleQuestionWrapper:not(.wq_questionAnswered) .wq_singleAnswerCtr.wq_IsPersonality', function( event ) {

            event.preventDefault();

            var $this				= $( this ),
                quizElem			= $this.closest( '.wq_quizCtr' ),
                resultsInfo			= JSON.parse( $this.find( '.wq_singleAnswerResultCtr' ).val() ),
                curQElem			= $this.closest( '.wq_singleQuestionWrapper' ),
                questionsAnswered	= parseInt( quizElem.data( 'questions-answered' ) ),
                totalQuestionsNum	= parseInt( quizElem.data( 'questions' ) ),
                animationIn			= quizElem.data( 'transition_in' ),
                forceAction			= parseInt( quizElem.data( 'force-action' ) ),
                isRetakeable		= parseInt( quizElem.data( 'retake-quiz' ) ),
                questionLayout		= quizElem.data( 'question-layout' ),
                autoScroll			= parseInt( quizElem.data( 'auto-scroll' ) );

            curQElem.addClass( 'wq_questionAnswered' );

            // Remove Any Points from Previous Selected Result if Any
            curQElem.find( '.wq_singleAnswerCtr.wq_answerSelected' ).each(function() {

                var oldResInfo = JSON.parse( $( this ).find( '.wq_singleAnswerResultCtr' ).val() );

                if ( '' !== oldResInfo ) {

                    oldResInfo.forEach(function( ele, ind, arr ) {

                        var resultElem		= quizElem.find( '.wq_singleResultWrapper[data-rid="' + ind + '"]' ),
                            resultPoints	= parseInt( resultElem.data( 'points' ) ) - parseInt( ele.points );

                        resultElem.data( 'points', resultPoints );
                    });
                }
            });

            // Add new Points
            if ( '' !== resultsInfo )	{

                resultsInfo.forEach(function( ele, ind, arr ) {

                    var resultElem    = quizElem.find( '.wq_singleResultWrapper[data-rid="' + ind + '"]' ),
                        resultPoints  = parseInt( resultElem.data( 'points' ) ) + parseInt( ele.points );

                    resultElem.data( 'points', resultPoints );
                });
            }

            // Increment Questions Answered
            if ( 1 === parseInt( curQElem.data( 'question-answered' ) ) ) {

                questionsAnswered++;
                curQElem.data( 'question-answered', 2 );
                quizElem.data( 'questions-answered', questionsAnswered );
            }

            $this.addClass( 'wq_answerSelected' );

            if ( 'single' === questionLayout ) {

                var curQ = parseInt( quizElem.data( 'current-question' ) );

                quizElem.data( 'current-question', curQ + 1 );

                if ( curQElem.next().length && 1 === autoScroll ) {
                    $( 'html, body' ).animate({
                        scrollTop: curQElem.next().offset().top - 75
                    }, 750 );
                }
            } else {
                curQElem.find( '.wq_btn-continue' ).trigger( 'click' );
            }

            if ( totalQuestionsNum !== questionsAnswered ) {
                return;
            }

            quizElem.find( '.wq_quizProgressValue' ).animate({ width: '100%' }).text( '100%' );
            $( 'html, body' ).animate({
                scrollTop: quizElem.find( '.wq_resultsCtr' ).offset().top - 75
            }, 750 );

            var resultElem	= null,
                maxPoints	= 0;

            quizElem.find( '.wq_singleResultWrapper' ).each(function() {

                var resultPoints = parseInt( $( this ).data( 'points' ) );

                if ( resultPoints > maxPoints ) {
                    maxPoints	= resultPoints;
                    resultElem	= $( this );
                }
            });

            $.post( quizElem.data( 'ajax-url' ), {
                action: 'wq_quizResults',
                rid: resultElem.data( 'rid' ),
                pid: parseInt( quizElem.data( 'quiz-pid' ) ),
                type: 'personality',
                _nonce: wq_l10n.nonce
            });

            if ( forceAction > 0 ) {

                var selector = (function() {

                    if ( 1 === forceAction ) {
                        return '.wq_quizEmailCtr';
                    }

                    if ( 2 === forceAction ) {
                        return '.wq_quizForceShareCtr';
                    }

                })();

                quizElem.find( '.wq_quizForceActionCtr' ).transition({
                    animation: animationIn,
                    onComplete: function() {
                        quizElem.find( selector ).transition({
                            animation: animationIn
                        });
                    }
                });

                resultElem.show();
                quizElem.find( '.wq_resultsCtr, .wq_retakeQuizCtr' ).hide();

            }

            resultElem.transition({ animation: animationIn });

            if ( isRetakeable ) {
                quizElem.find( '.wq_retakeQuizBtn' ).transition({ animation: animationIn });
            }
        });

        // FB Quiz

        var processQuiz = function( quizElem ) {

            quizElem.find( '.wq_singleQuestionWrapper .wq_loader-container' ).show();

            FB.api( '/me?fields=name,gender,first_name,last_name,email', function( res ) {

                var userInfo = res;
                quizElem.data( 'user-info', userInfo );

                FB.api( '/me/friends',  function( res ) {

                    userInfo.friends = res.data;

                    var data = {
                        action: 'wq_submitFbInfo',
                        pid: quizElem.data( 'quiz-pid' ),
                        user: userInfo,
                        profile: quizElem.data( 'quiz-profile' ),
                        _nonce: wq_l10n.nonce
                    };

                    $.post( quizElem.data( 'ajax-url' ), data, function( res ) {

                        if ( 2 === res.status ) {

                            var img = quizElem.find( '.wq_resultImg' );
                            img.attr( 'src', res.src );
                            img.load(function() {
                                quizElem.find( '.wq_resultDesc' ).html( res.desc );
                                quizElem.find( '.wq_singleQuestionWrapper.wq_IsFb, .wq_singleQuestionWrapper .wq_loader-container' ).hide( 'slow' );
                                quizElem.find( '.wq_singleResultWrapper.wq_IsFb' ).data( 'id', res.key ).show( 'slow' );
                            });
                        } else {

                            console.log( res.error );
                            setTimeout(function() {
                                window.location.href = quizElem.data( 'share-url' );
                            }, 200 );
                        }
                    });
                });
            });
        };

        window.getLogin = function( response ) {
            if ( 'connected' === response.status ) {
                var button = $( '.wq_questionLogin button' );
                button.removeClass( 'wq_loginFB' ).addClass( 'wq_playFB' );
            }
        };

        $( document ).on( 'click', '.wq_singleQuestionWrapper .wq_loginFB', function( event ) {

            event.preventDefault();

            var quizElem = $( this ).closest( '.wq_quizCtr' );

            FB.login(function() {
                FB.getLoginStatus(function( response ) {
                    if ( 'connected' === response.status ) {
                        processQuiz( quizElem );
                    } else {
                        setTimeout(function() {
                            window.location.href = quizElem.data( 'share-url' );
                        }, 200 );
                    }
                }, true );
            }, { scope: 'public_profile,email,user_friends' });
        });

        $( document ).on( 'click', '.wq_singleQuestionWrapper .wq_playFB', function( event ) {

            event.preventDefault();

            processQuiz( $( this ).closest( '.wq_quizCtr' ) );
        });

    })( jQuery );

</script>
<script>



    var h1 = document.getElementsByTagName('h1')[0],



        start = document.getElementById('start'),



        stop = document.getElementById('stop'),



        clear = document.getElementById('clear'),



        hiddenstore = document.getElementById('hiddenstore').value,



        cur_user = document.getElementById('cur_user').value,



        seconds = 0, minutes = 0, hours = 0,



        t;







    function add() {



        seconds++;








        h1.textContent = (seconds);



        var a= $('#hiddenstore').val(seconds);



        timer();







    }



    function timer() {



        t = setTimeout(add, 1);



    }



    timer();











    /* Start button */



    start.onclick = timer;







    /* Stop button */



    stop.onclick = function() {



        clearTimeout(t);

        var hiddenstore = document.getElementById('hiddenstore').value;
        var cur_user = document.getElementById('cur_user').value;
        var ajaxurl = 'http://diasimperdibles.com/wp-admin/admin-ajax.php';

        $.ajax({

            url: 'quiz/time',
            data: {
                hiddenstore: hiddenstore,
                _token: "{{ csrf_token() }}",
            },
            type: 'post',

            success: function (response){
                $('.wq_questionsCtr').hide();
                $('#stop').hide();
                $('#score_block').show();

                seconds = hiddenstore/1000;
                minutes = parseInt(seconds/60);

                document.getElementById('result_time').innerHTML = minutes+":"+seconds;
                setTimeout( function(){
                    window.location.href = "http://www.peugeot.com.mx";
                } ,5500)
            }

        });

    }







    /* Clear button */



    clear.onclick = function() {



        h1.textContent = "0";



        seconds = 0; minutes = 0; hours = 0;



    }







</script>
</body>
</html>