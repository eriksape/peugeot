<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['guest'])->group(function () {
    Route::get('/', function () {
        $vehicles = \App\Vehicle::orderBy('model')->get();
        $dealers = \App\Dealer::orderBy('branch')->get();
        return view('start')->with(compact('vehicles', 'dealers'));
    })->name('login');
});


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::post('register', 'Auth\RegisterController@register')->name('register');
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('register', 'Auth\RegisterController@register')->name('register');

Route::middleware(['auth', 'quiz'])->group(function () {
    Route::get('quiz', function(){
       return view('layouts.quiz');
    });
    Route::post('quiz/correct', function(\Illuminate\Http\Request $request){
        $user = \App\User::find(\Illuminate\Support\Facades\Auth::id());
        $user->correct_answers = $request->correct;
        $user->save();
        return 'SUCCESS!';
    });

    Route::post('quiz/time', function(\Illuminate\Http\Request $request){
        $user = \App\User::find(\Illuminate\Support\Facades\Auth::id());
        $user->quiz_time = $request->hiddenstore/1000;
        $user->save();
        return 0;
    });
});

Route::middleware(['auth'])->get('complete', function(){
    $user = \Illuminate\Support\Facades\Auth::user();
    return view('complete')->with('user', $user);
});


Route::get('terminos-y-condiciones', function(){
    return view('conditions');
})->name('conditions');

Route::get('dashboard', function(){
    return view('dashboard');
})->name('dashboard')->middleware(['auth','admin']);

Route::get('users/quiz', function(\Illuminate\Http\Request $request){
    $query = \App\User::with(['vehicle','dealer'])->where('type', 'user')
        ->whereNotNull('quiz_time');

    return \DataTables::eloquent($query)->make(true);
})->middleware(['auth','admin']);