<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class QuizMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user()->type === 'admin') {
            return redirect('/dashboard');
        }

        if (Auth::user()->quiz_time){
            return redirect('/complete');
        }

        return $next($request);
    }
}
