<?php

namespace App\Console\Commands;

use App\Dealer;
use App\User;
use App\Vehicle;
use Illuminate\Console\Command;

class ProjectFill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dealers = '[
              {
                "DISTRIBUIDOR": "PEUGEOT  TIJUANA",
                "CIUDAD": "TIJUANA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT MEXICALI",
                "CIUDAD": "MEXICALI"
              },
              {
                "DISTRIBUIDOR": "LYON MOTORS DURANGO",
                "CIUDAD": "DURANGO"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT HERMOSILLO",
                "CIUDAD": "HERMOSILLO"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT CHIHUAHUA",
                "CIUDAD": "CHIHUAHUA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT CULIACAN",
                "CIUDAD": "CULIACAN"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT CHIAPAS",
                "CIUDAD": "TUXTLA GUTIERREZ"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT OAXACA",
                "CIUDAD": "OAXACA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT CAMPECHE",
                "CIUDAD": "CAMPECHE"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT QUERETARO",
                "CIUDAD": "QUERETARO"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT PACHUCA",
                "CIUDAD": "PACHUCA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT PUEBLA",
                "CIUDAD": "PUEBLA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT SAN LUIS POTOSI",
                "CIUDAD": "SAN LUIS POTOSI"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT LEON",
                "CIUDAD": "LEON"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT FELINAUTOS BOCA",
                "CIUDAD": "BOCA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT XALAPA",
                "CIUDAD": "XALAPA VERACRUZ"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT AGUASCALIENTES",
                "CIUDAD": "AGUSCALIENTES"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT TOLUCA",
                "CIUDAD": "TOLUCA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT SALTILLO",
                "CIUDAD": "SALTILLO"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT GALOJAL",
                "CIUDAD": "GUADALAJARA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT MORELIA",
                "CIUDAD": "MORELIA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT VEGSA",
                "CIUDAD": "GUADALAJARA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT REFRANCE GONZALITOS",
                "CIUDAD": "MONTERREY"
              },
              {
                "DISTRIBUIDOR": "EUROSURMAN TEC",
                "CIUDAD": "MONTERREY"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT REFRANCE SAN NICOLAS",
                "CIUDAD": "MONTERREY"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT PUERTO VALLARTA",
                "CIUDAD": "PUERTO VALLARTA"
              },
              {
                "DISTRIBUIDOR": "MERIDIEN MERIDA",
                "CIUDAD": "MERIDA"
              },
              {
                "DISTRIBUIDOR": "MERIDIEN CANCUN",
                "CIUDAD": "CANCUN"
              },
              {
                "DISTRIBUIDOR": "LYON MOTORS TORREÓN",
                "CIUDAD": "TORREON"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT COLIMA",
                "CIUDAD": "COLIMA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT VILLAHERMOSA",
                "CIUDAD": "VILLAHERMOSA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT CUERNAVACA",
                "CIUDAD": "CUERNAVACA"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT ARBOLEDAS",
                "CIUDAD": "CDMX ARBOLEDAS"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT INTERLOMAS",
                "CIUDAD": "CDMX INTERLOMAS"
              },
              {
                "DISTRIBUIDOR": "ALDEN ROILION",
                "CIUDAD": "CDMX LOMAS VERDES"
              },
              {
                "DISTRIBUIDOR": "AFU UNIVERSIDAD",
                "CIUDAD": "CDMX UNIVERSIDAD"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT VALLEJO",
                "CIUDAD": "CDMX VALLEJO"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT ANZURES",
                "CIUDAD": "CDMX ANZURES"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT MIRAMONTES",
                "CIUDAD": "CDMX MIRAMONTES"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT TAMPICO",
                "CIUDAD": "TAMAULIPAS"
              },
              {
                "DISTRIBUIDOR": "PEUGEOT LOMAS VERDES",
                "CIUDAD": ""
              }
            ]';
        $dealers = json_decode ($dealers);

        $vehicles = [
            '208',
            '301',
            '308',
            'Expert',
            'Manager',
            'Nueva SUV 2008',
            'Nueva SUV 3008',
            'Nueva SUV 5008',
            'Partner',
            'Partner Tepee',
            'Traveller'
        ];

        foreach ($dealers as $row)
        {
            $dealer = new Dealer();
            $dealer->branch = $row->DISTRIBUIDOR;
            $dealer->city = $row->CIUDAD;
            $dealer->save();
        }

        foreach ($vehicles as $row)
        {
            $vehicle = new Vehicle();
            $vehicle->model = $row;
            $vehicle->save();
        }

        $user = new User();
        $user->name = 'Administrador';
        $user->email = env('ADMIN_EMAIL');
        $user->password = bcrypt('$root$.2018');
        $user->type = 'admin';
        $user->save();
    }
}

